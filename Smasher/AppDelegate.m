//
//  AppDelegate.m
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>

#import "MISplashViewController.h"
#import "MILoadingViewController.h"
#import "MIHomeViewController.h"
#import "MIRightPanelViewController.h"
#import "MILoginViewController.h"
#import "MILoginPopView.h"
#import "MICustomDatePicker.h"
#import "MIRegisterViewController.h"
#import "MIChangeLanguageViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //....
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    NSLog(@"didFinishLaunchingWithOptions");
    
    [[MIAFNetworking sharedInstance] setDisableTracing:YES];
    
    if (IS_IPHONE_SIMULATOR)
        [[MIAFNetworking sharedInstance] setDisableTracing:NO];
    
     _isSpanish = [[CUserDefaults objectForKey:UserDefaultCurrentLanguage] isEqual:ClanguageSpanish];
    
    if (_isSpanish)
        [[LocalizationSystem sharedLocalSystem] setLanguage:ClanguageSpanish];
    else
        [[LocalizationSystem sharedLocalSystem] setLanguage:ClanguageEnglish];
    
    [self initializeWithLaunchOptions:launchOptions];
    [self initiateSplashScreen];
    
    [[APIRequest request] loadGeneralData_completed:nil];
    [[APIRequest request] loadCountryList_completed:nil];
   
    [self.window makeKeyAndVisible];
    return YES;
}


#pragma mark -
#pragma mark - General Method

- (void)initializeWithLaunchOptions:(NSDictionary *)launchOptions
{
     
    //.....Monitoring Internet Reachability.....
    AFNetworkActivityIndicatorManager.sharedManager.enabled = YES;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         if (status == AFNetworkReachabilityStatusNotReachable)
         {
         }
     }];
    
    
    //.....
}

- (void)initiateSplashScreen
{
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    MISplashViewController *splashVC = [[MISplashViewController alloc] initWithNibName:@"MISplashViewController" bundle:nil];
    [self.window setRootViewController:splashVC];
    
    if ([CUserDefaults valueForKey:UserDefaultCurrentLanguage])
        [self performSelector:@selector(initiateLoadingScreen) withObject:nil afterDelay:3];
    else
        [self performSelector:@selector(selectLanguge) withObject:nil afterDelay:3];
}

- (void)initiateLoadingScreen
{
    MILoadingViewController *loadinVC = [[MILoadingViewController alloc] initWithNibName:@"MILoadingViewController" bundle:nil];
    [self.window setRootViewController:loadinVC];
    [[APIRequest request] visitores_completed:nil];
    [self performSelector:@selector(initiateApplicationRoot) withObject:nil afterDelay:3];
}
- (void)selectLanguge
{
    MIChangeLanguageViewController *languageVC = [[MIChangeLanguageViewController alloc] initWithNibName:@"MIChangeLanguageViewController" bundle:nil];
    [self.window setRootViewController:languageVC];
}
- (void)initiateApplicationRoot
{
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    if ([UIApplication userId])
    {
        NSLog(@"initiateApplicationRoot signinUser");
        appDelegate.loginUser = (TBLUser *)[TBLUser findOrCreate:@{@"user_id":[UIApplication userId]}];
        [[APIRequest request] userProfile_completed:nil];
        
    }
    
    [self signinUser];
   

}

- (void)signinUser
{
    _sidePanelViewController = nil;
    [self setWindowRootVC:self.sidePanelViewController animated:YES completion:nil];
}

- (void)logoutUser
{
    //....General Logout
    appDelegate.loginUser = nil;
    [UIApplication removeUserId];
    [CUserDefaults removeObjectForKey:UserDefaultLoginToken];
    [CUserDefaults synchronize];
    [self disableRemoteNotification];
    
    //....
    MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
    [appDelegate setWindowRootVC:[UINavigationController navigationControllerWithRootViewController:loginVC] animated:YES completion:^(BOOL finished)
    {
        _sidePanelViewController = nil;
    }];
}
- (void)logoutUserFromSideMenu
{
    //....General Logout
    appDelegate.loginUser = nil;
    [UIApplication removeUserId];
    [CUserDefaults removeObjectForKey:UserDefaultLoginToken];
    [CUserDefaults synchronize];
    [self disableRemoteNotification];
    
    //[self popToHomeViewController];
    [self signinUser];
}

- (void)popToHomeViewController
{
    [self signinUser];
    return;
    
//    MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
//    
//    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:homeVC]];
//    [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:nil];
}
#pragma mark -
#pragma mark - Helper Method

- (void)showLoginRequirePopUp:(UIViewController*)viewController
{
    __block MILoginPopView *login = [[MILoginPopView alloc] initWithInfo:@"" handler:^(NSInteger option)
     {
         [viewController dismissPopUp:login];
         
         switch (option)
         {
             case 1:
             {
                 //...Login
                 [self logoutUser];
                 break;
             }

             case 2:
             {
                 //...Create Account
                 
                 [self showSelectDOBPopUp:viewController];
                 break;
             }

         }
         
     }];
    [viewController presentPopUp:login from:PresentTypeCenter shouldCloseOnTouchOutside:NO];
}

- (void)showSelectDOBPopUp:(UIViewController*)viewController
{
    __block MICustomDatePicker *picker = [[MICustomDatePicker alloc]initWithDate:nil handler:^(NSDate *date)
         {
             [viewController dismissPopUp:picker];
             
             if(date)
             {
                 MIRegisterViewController *signUpVC = [[MIRegisterViewController alloc] initWithNibName:@"MIRegisterViewController" bundle:nil];
                 [signUpVC setIObject:date];
                 signUpVC.isFromHome = [viewController isKindOfClass:[MIHomeViewController class]];
                 [viewController.navigationController pushViewController:signUpVC animated:YES];
             }
         }];
    
    [viewController presentPopUp:picker from:PresentTypeCenter shouldCloseOnTouchOutside:NO];
}

- (NSURL *)imageURL:(NSString *)url withSize:(CGSize)size
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@", url]];
    //return [NSURL URLWithString:[NSString stringWithFormat:@"%@?width=%d&height=%d", url, (int)size.width*2, (int)size.height*2]];
}
#pragma mark -
#pragma mark - Root & Main

- (LGSideMenuController *)sidePanelViewController
{
    if (!_sidePanelViewController)
    {
        MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];

        MIRightPanelViewController *rightVC = [[MIRightPanelViewController alloc] initWithNibName:@"MIRightPanelViewController" bundle:nil];
        
        _sidePanelViewController = [LGSideMenuController sideMenuControllerWithRootViewController:[UINavigationController navigationControllerWithRootViewController:homeVC]  leftViewController:nil  rightViewController:rightVC];
    
        
        _sidePanelViewController.rightViewWidth = IS_IPAD ? (CScreenWidth*0.6) : (CScreenWidth/3)*2;
        _sidePanelViewController.leftViewPresentationStyle = LGSideMenuPresentationStyleSlideAbove;
    }
    
    return _sidePanelViewController;
}

- (BOOL)sidePanelViewControllerInitiate
{
    return (_sidePanelViewController != nil);
}

- (void)setWindowRootVC:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed
{
    [UIView transitionWithView:self.window
                      duration:animated? 0.3 : 0.0
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         
         self.window.rootViewController = vc;
         [UIView setAnimationsEnabled:oldState];
     } completion:^(BOOL finished)
     {
         if (completed)
             completed(finished);
     }];
}


#pragma mark -
#pragma mark - (APNS)Remote Notification

- (void)enableRemoteNotification
{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max)
    {
        NSLog(@"Version 8 : Push");
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        UNAuthorizationOptions authOptions =
        (UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge);
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
    }
}

- (void)disableRemoteNotification
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Unable to register for remote notifications: %@", error);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    const unsigned *devTokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *token = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                       ntohl(devTokenBytes[0]), ntohl(devTokenBytes[1]), ntohl(devTokenBytes[2]),
                       ntohl(devTokenBytes[3]), ntohl(devTokenBytes[4]), ntohl(devTokenBytes[5]),
                       ntohl(devTokenBytes[6]), ntohl(devTokenBytes[7])];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Token: %@", token);
    
    if (token)
    {
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        [application registerForRemoteNotifications];
        NSLog(@"didRegisterUser");
    }
}


#pragma mark -
#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}


@end

//
//  main.m
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Mac-00014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

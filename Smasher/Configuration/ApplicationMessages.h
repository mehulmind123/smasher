//
//  ApplicationMessages.h
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//


#ifndef ApplicationMessages_h
#define ApplicationMessages_h

/*======== Loader Messages =========*/
#define CMessageCongratulation              @"Congratulations!"
#define CMessageSorry                       @"Sorry!"
#define CMessageLoading                     @"Loading..."
#define CMessageSearching                   @"Searching..."
#define CMessageVerifying                   @"Verifying..."
#define CMessageWait                        @"Please Wait..."
#define CMessageUpdating                    @"Updating..."
#define CMessageAuthenticating              @"Authenticating..."
#define CMessageErrorInternetNotAvailable   @"Intenet Connection Not Available!\n Please Try Again Later."
//#define CMessageNoResultFound               @"Nothing to see here yet!"
#define CMessageSearchNoResultFound         @"Sorry, there are no characters available for your search.\nKeep on smashing to fine more!"
#define CMessageNoResultFound               @"Sorry, there are no characters available."
#define CErrorTapToRetry                    @"Something wrong here...\ntap anywhere to refresh the page."



#define CYes                    @"Yes"
#define CNo                     @"No"





/*======== LRF =========*/

#define CMessageBlankEmail                  @"Please enter your email address."
#define CMessageValidEmail                  @"Please enter valid email address."
#define CMessageEmailAndParentEmailSame     @"Please use a different email for Parent Email and User Email."
#define CMessageBlankParentEmail            @"Please enter your parent's email address."
#define CMessageBlankPassword               @"Please enter your password."
#define CMessageAgePermission               @"Select box to confirm you are over the age of 16 years."
#define CMessageTermsOfUse                  @"Select box to confirm that you consent to our Privacy Policy and Terms of Use."
#define CMessageEmailNotRegister            @"This email address is not registered with us."
#define CMessageEmailPasswordNotReconize    @"Email address or Password does not match"
#define CMessageVerifyAccount               @"Please activate your account."


#define CMessageBlankUserName               @"Please enter your name."
#define CMessageEnterPassword               @"Please enter password."
#define CMessageEnterConfirmPassword        @"Please enter confirm password."
#define CMessageAlphaNumbricPassword        @"Password must be minimum 6 character alphanumeric."
#define CMessageConfirmPasswordNotMatch     @"Password and confirm password does not match."
#define CMessageSelectCountry               @"Please select country."
#define CMessageActivateEmail               @"Please activate your email address."
#define CMessageSelectDOB                   @"Please select date of birth."
#define CMessageEnterOldPassword            @"Please enter old password."
#define CMessageEnterNewPassword            @"Please enter new password."
#define CMessagePasswordMisMatch            @"New password and Confirm password must be same."


//#define CMessageLoginRequire                @"An account is required to add characters to lists of Smasher Collector you own or want."

#define CMessageLoginRequire                @"An account is required to add characters to your Smashers collection. Please create an account or sign in to get smashing!"
#define CMessageLogout                      @"Are you sure you want to logout?"




#endif /* ApplicationMessages_h */

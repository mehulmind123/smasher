//
//  ApplicationConstants.h
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//


#ifndef ApplicationConstants_h
#define ApplicationConstants_h


/*======== WHITE SPACE CHAR SET =========*/
#define CWhitespaceCharSet [NSCharacterSet whitespaceAndNewlineCharacterSet]

//#define NSLog(...)

/*======== FONT =========*/
#define CFontHelveticaRegular(fontSize) [UIFont fontWithName:@"HelveticaNeue" size:fontSize]
#define CFontHelveticaBold(fontSize)    [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize]
#define CFontHelveticaMedium(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize]
#define CFontHelveticaLight(fontSize)   [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize]
#define CFontHelveticaItalic(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Italic" size:fontSize]

#define CFontSolidoBook(fontSize)               [UIFont fontWithName:@"SolidoCondensed-Book" size:fontSize]
#define CFontSolidoMedium(fontSize)             [UIFont fontWithName:@"SolidoCondensed-Medium" size:fontSize]
#define CFontSolidoBold(fontSize)               [UIFont fontWithName:@"SolidoCondensed-Bold" size:fontSize]
#define CFontSolidoExtraBold(fontSize)          [UIFont fontWithName:@"SolidoCondensed-ExtraBold" size:fontSize]
#define CFontSolidoLight(fontSize)              [UIFont fontWithName:@"SolidoCondensed-Light" size:fontSize]
#define CFontSolidoUltraLight(fontSize)         [UIFont fontWithName:@"SolidoCondensed-UltraLight" size:fontSize]
#define CFontSolidoUltraLightItalic(fontSize)   [UIFont fontWithName:@"SolidoCondensed-UltraLightItalic" size:fontSize]

#define CFontRobotoRegular(fontSize)    [UIFont fontWithName:@"Roboto-Regular" size:fontSize]
#define CFontRobotoBold(fontSize)       [UIFont fontWithName:@"Roboto-Bold" size:fontSize]
#define CFontRobotoMedium(fontSize)     [UIFont fontWithName:@"Roboto-Medium" size:fontSize]
#define CFontRobotoLight(fontSize)      [UIFont fontWithName:@"Roboto-Light" size:fontSize]
#define CFontRobotoThin(fontSize)       [UIFont fontWithName:@"Roboto-Thin" size:fontSize]


#define CFontGroBold(fontSize)          [UIFont fontWithName:@"GROBOLD" size:fontSize]
#define CFontHalveticaNeueLT(fontSize)  [UIFont fontWithName:@"HelveticaNeueLTPro-BdEx" size:fontSize]




/*======== COLOR =========*/
#define ColorWhite_FFFFFF               CRGB(255, 255, 255)

#define ColorBlack_000000                CRGB(0, 0, 0)

#define ColorBlue_0E6EB8                 CRGB(44, 110, 184)
#define ColorBlue_45CFEF                 CRGB(69, 207, 239)
#define ColorBlue_117EAB                 CRGB(42, 126, 171)
#define ColorBlue_1493B3                 CRGB(47, 147, 179)
#define ColorBlue_9EF0FF                 CRGB(158, 240, 255)
#define ColorBlue_6BCBF3                 CRGB(107, 203, 243)
#define ColorBlue_3B8DF2                 CRGB(59, 141, 242)
#define ColorBlue_64E1FC                 CRGB(100, 225, 252)
#define ColorBlue_205391                 CRGB(32, 83, 145)

#define ColorYellow_FFF100               CRGB(255, 241, 0)
#define ColorRed_E93429                  CRGB(233, 52, 41)
#define ColorRed_D12015                  CRGB(193, 24, 13)

#define ColorGray_F0F0F0                 CRGB(240, 240, 240)
#define ColorGray_DADADA                 CRGB(218, 218, 218)
#define ColorGray_E5E5E5                 CRGB(229, 229, 229)
#define ColorGray_857E7E                 CRGB(133, 126, 126)

#define ColorBlue_2235A0                 CRGB(34, 53, 160)



//.....
#define ColorBlack_202020               CRGB(32, 32, 32)

#define ColorGray_666666                CRGB(102, 102, 102)

#define ColorGray_908D8D                CRGB(144, 141, 141)
#define ColorGray_989898                CRGB(152, 152, 152)
#define ColorGray_BBBBBB                CRGB(187, 187, 187)
#define ColorGray_CCCCCC                CRGB(204, 204, 204)

#define ColorRed_EF4545                 CRGB(239, 69, 69)

#define ColorBlue_0064ED                CRGB(0, 100, 237)
#define ColorBlue_00AAED                CRGB(0, 170, 237)
#define ColorBlue_007FD4                CRGB(0, 127, 212)
#define ColorBlue_0090F3                CRGB(0, 144, 243)
#define ColorBlue_1190E7                CRGB(17, 144, 231)
#define ColorBlue_3490C2                CRGB(52, 144, 194)
#define ColorBlue_2F7AC1                CRGB(47, 122, 193)

#define ColorBlue_3576E9                CRGB(53, 118, 233)

#define ColorBlueSky_06A8F1             CRGB(6, 168, 241)
#define ColorBlueSky_1DB4FF             CRGB(29, 180, 255)
#define ColorBlueSky_318DAF             CRGB(58, 154, 187)
#define ColorBlueSky_249CFE             CRGB(36, 156, 254)
#define ColorBlueSky_56CEE8             CRGB(93, 221, 214)
#define ColorBlueSky_4DC0FA             CRGB(77, 192, 250)
#define ColorBlueSky_409DFE             CRGB(64, 157, 254)
#define ColorBlueSky_82C2FB             CRGB(130, 194, 251)
#define ColorBlueSky_9DC3EA             CRGB(157, 195, 234)


#define ColorPink_5F185A                CRGB(87, 14, 80)
#define ColorPink_CA71F3                CRGB(202, 113, 243)
#define ColorPink_F371D0                CRGB(243, 113, 208)
#define ColorPink_BB3DF0                CRGB(187, 61, 240)
#define ColorPink_CE41A9                CRGB(206, 65, 169)
#define ColorPink_F09CE6                CRGB(228, 156, 239)
#define ColorPinkTone_B39A9A            CRGB(179, 154, 154)
#define ColorPinkTone_F48F9D            CRGB(244, 143, 157)
#define ColorPinkTone_F9BBB6            CRGB(249, 187, 182)
#define ColorPinkTone_D36B7A            CRGB(211, 107, 122)

#define ColorGreen_24E9A6               CRGB(36, 233, 166)
#define ColorGreen_3ABD83               CRGB(58, 189, 131)
#define ColorGreen_247D62               CRGB(36, 125, 98)

#define ColorOrange_FBC28B              CRGB(251, 194, 139)
#define ColorOrange_D28C61              CRGB(210, 140, 97)
#define ColorOrange_EF9138              CRGB(239, 145, 56)

#define Color1_5B7EF4                   CRGB(91, 126, 244)
#define Color2_F671D2                   CRGB(246, 113, 210)
#define Color3_3FCF7D                   CRGB(63, 207, 125)
#define Color4_F66CAD                   CRGB(246, 108, 173)
#define Color5_F69016                   CRGB(246, 144, 22)
#define Color6_2B2B2B                   CRGB(43, 43, 43)
#define Color7_F42A67                   CRGB(244, 42, 103)
#define Color8_F8CA04                   CRGB(248, 202, 4)
#define Color9_6164CB                   CRGB(97, 100, 203)
#define Color10_2012BB                  CRGB(32, 18, 187)




/*======== General Constant =========*/
#define CMinimumAge                             13
#define UserDefaultCMS                          @"CMSData"


/*======== USER DEFAULT =========*/
#define UserDefaultLoginToken                   @"LoginToken"
#define UserDefaultCMSAboutSmashPoint           @"CMSAboutSmashPoint"
#define UserDefaultCMSAboutUs                   @"CMSAboutUs"
#define UserDefaultCMSContactUs                 @"CMSContactUs"
#define UserDefaultCMSPrivacyPolicy             @"CMSPrivacyPolicy"
#define UserDefaultCountryList                  @"CountryList"
#define UserDefaultGeneralData                  @"GeneralData"
#define UserDefaultCurrentLanguage              @"CurrentLanguage"


/*======== NOTIFICATION CONSTANTS =========*/

#define CFooterVisitZuruUrl                     @"http://www.zuru.com"
#define CFooterKindsSafetyUrl                   @"http://www.zuru.com/wp-content/uploads/2013/01/Parent_Doc.pdf"
#define CFooterWhereToBuyUrl                    @"http://www.zuru.com/zuru-store"
#define CFooterContactUsUrl                     @"http://www.zuru.com/zuru-contacts"
#define CSmasherWorld_Com                       @"http://www.smashersworld.com"


/*======== MAILCHIP CONSTANTS =========*/
#define CMAILCHIP_SUBSCRIBER_API_KEY            @"814ba3eb89738e0b408b452266398cae-us15"
#define CMAILCHIP_SUBSCRIBER_ID                 @"1e1ad669bb"
#define CMAILCHIP_BASEURL                       @"https://us15.api.mailchimp.com/2.0/lists/subscribe.json?"


/*======== NOTIFICATION CONSTANTS =========*/

#define NotificationLeftPanelWillShow           @"LeftPanelWillShow"


/*======== OTHER =========*/
#define CPasswordLength     6

#define ClanguageEnglish              @"eng"
#define ClanguageSpanish              @"es"

/*======== LANGUAGE =========*/
#define CLocalize(text)             [[LocalizationSystem sharedLocalSystem] localizedStringForKey:text value:text]

#endif /* ApplicationConstants_h */

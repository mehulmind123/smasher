//
//  APIRequest.m
//  Smasher
//
//  Created by Mac-00014 on 04/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "APIRequest.h"

static APIRequest *request = nil;

@implementation APIRequest

+ (id)request
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
                  {
                      request = [[APIRequest alloc] init];
                      [[MIAFNetworking sharedInstance] setBaseURL:BASEURL];
                      
                      NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:[[[[MIAFNetworking sharedInstance] sessionManager] responseSerializer] acceptableStatusCodes]];
                      
                      [indexSet addIndex:400];
                      [indexSet addIndex:401];
                      [indexSet addIndex:402];
                      [indexSet addIndex:503];
                      
                      if(IS_IPHONE_SIMULATOR)
                      {

                          [indexSet addIndex:201];

                          [indexSet addIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(300, 300)]];
                      }
                      
                      
                      
                      [[[MIAFNetworking sharedInstance] sessionManager] responseSerializer].acceptableStatusCodes = indexSet;
                      
                  });
    
    return request;
}



- (void)failureWithAPI:(NSString *)api andError:(NSError *)error
{
    NSLog(@"API Error(%@) == %@", api, error);
    
    if(IS_IPHONE_SIMULATOR)
    {
        if (error.code != -999 && ![error.localizedDescription isEqualToString:@"cancelled"] && ![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
            [CustomAlertView iOSAlert:@"" withMessage:error.localizedDescription onView:[UIApplication topMostController]];
    }
}

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler
{
    NSLog(@"%@ API Error == %@", api, error);
    
    if (error.code != -999 && ![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
    {
        
        [[UIApplication topMostController] alertWithAPIErrorTitle:@"ERROR!" message:CLocalize(@"An error has occured. Please check your network connection or try again.") handler:^(NSInteger index, NSString *btnTitle)
         {
             
             if (handler) handler(index == 0);
             
         }];
    }
}

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task
{
    return ((NSHTTPURLResponse *)task.response).statusCode == 200 ? nil : [NSError errorWithDomain:@"" code:((NSHTTPURLResponse *)task.response).statusCode userInfo:nil];
}

- (BOOL)isJSONDataValidWithResponse:(id)response
{
    id data = [response valueForKey:CJsonData];
    
    if (!data)
    {
        return [self isJSONStatusValidWithResponse:response];
    }
    
    
    if ([data isEqual:[NSNull null]]) {
        return NO;
    }
    
    if ([data isKindOfClass:[NSString class]]) {
        if (((NSString *)data).length == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSArray class]]) {
        if (((NSArray *)data).count == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        if (((NSDictionary *)data).count == 0)
            return NO;
    }
    
    return [self isJSONStatusValidWithResponse:response];
}

- (BOOL)isJSONStatusValidWithResponse:(id)response
{
    if (!response)
        return NO;
    
    NSNumber *status = [[response valueForKey:CJsonMeta] numberForInt:CJsonStatusCode];
    
    if ([@CStatusTwoHundred isEqual:status])
        return YES;
    else if ([status isEqual:@CStatusFourHundred] && ![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
    {
        [CustomAlertView iOSAlert:@"" withMessage:[[response valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:[UIApplication topMostController]];
        
        return NO;
    }
    
    return NO;
    
}
- (BOOL)isUserNotVerifiedWithResponse:(id)response
{
    if (!response)
        return NO;
    
    NSNumber *status = [response numberForInt:CJsonStatusCode];
    
    return [status isEqual:@CStatusFourHundredTwo];
    
}

- (BOOL)checkResponseStatus:(id)response
{
    switch ([[response valueForKey:CJsonMeta] intForKey:CJsonUserStatus])
    {
        case CStatusTwo:
        {
            //...Inactive User
            if ([UIApplication userId])
            {
                [appDelegate logoutUserFromSideMenu];
                
                if(![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
                    [CustomAlertView iOSAlert:@"Inactive User" withMessage:nil onView:[UIApplication topMostController]];
            }
            
            return YES;
            break;
        }
        case CStatusNine:
        {
            //...Under Maintenance
            [appDelegate logoutUserFromSideMenu];
            return YES;
            break;
        }
        default:
        {
            return NO;
            break;
        }
    }
}

- (BOOL)isActiveUserHTTPStatusWithDataTask:(NSURLSessionDataTask *)task withResponse:(id)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
    
    if (httpResponse.statusCode == CStatusFourHundredOne)
    {
        //...Inactive User , delete user , Token expired
        if ([UIApplication userId])
        {
            [appDelegate logoutUser];
            
            if(![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
            [CustomAlertView iOSAlert:@"Inactive User" withMessage:nil onView:[UIApplication topMostController]];
        }
        
        return NO;
    }

    return YES;
}


- (BOOL)isSuccessHTTPStatusWithDataTask:(NSURLSessionDataTask *)task withResponse:(id)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
    
    if (httpResponse.statusCode == CStatusFourHundredOne)
    {
        //...Inactive User , delete user , Token expired
        if ([UIApplication userId])
        {
            [appDelegate logoutUserFromSideMenu];
            
            if(![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
                [CustomAlertView iOSAlert:@"Inactive User" withMessage:nil onView:[UIApplication topMostController]];
        }
        
        return NO;
    }
    else if (![@[@CStatusTwoHundred,@CStatusFourHundredTwo] containsObject:[NSNumber numberWithInteger:httpResponse.statusCode]])
    {
        if(![[UIApplication topMostController] isKindOfClass:[UIAlertController class]])
            
            [CustomAlertView iOSAlert:@"" withMessage:[response stringValueForJSON:CJsonMessage] onView:[UIApplication topMostController]];
        
        return NO;
        
    }
    //    else if([self checkResponseStatus:response])
    //            return NO;
    
    
    return YES;
}

- (NSInteger)paginationWithResponse:(id)response
{
    if(!response)
        return 1;
    
    if([[[response valueForKey:CJsonMeta] valueForKey:CJsonPagination] integerForKey:@"current_page"] >= [[[response valueForKey:CJsonMeta] valueForKey:CJsonPagination] integerForKey:@"total_pages"])
    return 1;
    
    return [[[response valueForKey:CJsonMeta] valueForKey:CJsonPagination] integerForKey:@"current_page"]+1;
}

- (AFHTTPSessionManager *)sessionManager
{
    AFHTTPSessionManager  *manager = [[AFHTTPSessionManager manager] initWithBaseURL:[NSURL URLWithString:CMAILCHIP_BASEURL]];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    
    [manager.requestSerializer setValue:[UIApplication performSelectorFromStringWithReturnObject:@"uniqueIdentifier"] forHTTPHeaderField:@"Device-Identifier"];
    
    [manager.requestSerializer setValue:[UIApplication performSelectorFromStringWithReturnObject:@"appID"] forHTTPHeaderField:@"Applciaiton-Identifier"];
    [manager.requestSerializer setValue:[UIApplication performSelectorFromStringWithReturnObject:@"accessToken"] forHTTPHeaderField:@"Access-Token"];
    [manager.requestSerializer setValue:[UIApplication performSelectorFromStringWithReturnObject:@"userId"] forHTTPHeaderField:@"User-Identifier"];
    
    return manager;
}


#pragma mark -
#pragma mark - General

- (void)loadGeneralData_completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagGeneralDetails parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             if ([self isJSONDataValidWithResponse:responseObject])
             {
                 [CUserDefaults setValue:[responseObject valueForKey:CJsonData] forKey:UserDefaultGeneralData];
                 [CUserDefaults synchronize];
             }
             
             if (completion)
                 completion(responseObject, nil);
             
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagGeneralDetails andError:error];
         
         if (completion)
             completion(nil, error);
     }];
    
    
}

- (void)loadCMS:(NSInteger)type completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagCMSDetails parameters:@{@"type":[NSNumber numberWithInteger:type]} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isActiveUserHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                 switch (type)
                 {
                         case 1:
                     {
                         [CUserDefaults setValue:[responseObject valueForKey:CJsonData] forKey:UserDefaultCMSAboutSmashPoint];
                         break;
                     }
                         case 2:
                     {
                         [CUserDefaults setValue:[responseObject valueForKey:CJsonData] forKey:UserDefaultCMSAboutUs];
                         break;
                     }
                         
                         case 3:
                     {
                         [CUserDefaults setValue:[responseObject valueForKey:CJsonData] forKey:UserDefaultCMSContactUs];
                         break;
                     }
                         
                         case 4:
                     {
                         [CUserDefaults setValue:[responseObject valueForKey:CJsonData] forKey:UserDefaultCMSPrivacyPolicy];
                         break;
                     }
                         
                 }
                 [CUserDefaults synchronize];
             }
             
             if(completion)
             completion(responseObject, nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagCMSDetails andError:error];
         if (completion)
             completion(nil, error);
     }];
}

- (void)loadCountryList_completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagCountries parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             if([[APIRequest request]isJSONDataValidWithResponse:responseObject])
             {
                 [CUserDefaults setValue:[responseObject valueForKey:CJsonData] forKey:UserDefaultCountryList];
                 [CUserDefaults synchronize];
             }
             
             if(completion)
                 completion(responseObject, nil);
             
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagCountries andError:error];
         if (completion)
             completion(nil, error);
     }];
}


- (void)visitores_completed:(void (^)(id responseObject, NSError *error))completion
{
    NSString *strUUID = [self getUniqueDeviceIdentifierAsString];
    if(!strUUID.isBlankValidationPassed)
    {
        return;
    }
    NSDictionary *dict = @{@"token":strUUID,
                           @"device_type":@"ios"};
    [[MIAFNetworking sharedInstance] POST:CAPITagVisitores parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
//         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
//         {
             if(completion)
                 completion(responseObject, nil);
//
//         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagVisitores andError:error];
         if (completion)
             completion(nil, error);
     }];
}

-(NSString *)getUniqueDeviceIdentifierAsString
{
    NSString *strApplicationUUID = [UIApplication keychainForKey:@"app_UUID"];
    if (strApplicationUUID == nil)
    {
        strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [UIApplication setKeychain:strApplicationUUID forKey:@"app_UUID"];
    }
    
    return strApplicationUUID;
}

#pragma mark -
#pragma mark - LRF

- (void)signInWithEmail:(NSString *)email andPassword:(NSString *)password completed:(void (^)(id responseObject, NSError *error))completion
{
    
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email,
                           @"password":password};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagSignIn parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if([[APIRequest request] isUserNotVerifiedWithResponse:responseObject])
             {
                 [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:[UIApplication topMostController]];
                                  
             }
             else if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                  completion(responseObject, nil);
                 [self saveLoginUserToLocal:responseObject];
             }
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagSignIn andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self signInWithEmail:email andPassword:password completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
    
}

- (void)signupWithEmail:(NSString *)email andPassword:(NSString *)password andUserName:(NSString *)userName andDOB:(NSString*)dob andParentEmail:(NSString*)parentEmail andCountry:(NSNumber*)countryId andSignUpToNewsLetter:(BOOL)isSignUpToNewsLetter completed:(void (^)(id responseObject, NSError *error))completion
{
    
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email,
                           @"password":password,
                           @"username":userName,
                           @"date_of_birth":dob,
                           @"parent_email":parentEmail,
                           @"country_id":countryId,
                           @"newsletter":isSignUpToNewsLetter ? @"Yes" : @"No"};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagSignup parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if(completion)
             completion(responseObject, nil);
             
             [self subscribeMailChipEmail:email andUserName:userName completed:completion];
         }
         
        
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagSignup andError:error andHandler:^(BOOL retry)
          {
              if(retry)
              [self signupWithEmail:email andPassword:password andUserName:userName andDOB:dob andParentEmail:parentEmail andCountry:countryId andSignUpToNewsLetter:isSignUpToNewsLetter completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
}

- (void)subscribeMailChipEmail:(NSString *)email andUserName:(NSString *)userName completed:(void (^)(id responseObject, NSError *error))completion
{
    
    NSDictionary *dict = @{@"merge_vars[FNAME]":userName,
                           @"apikey":CMAILCHIP_SUBSCRIBER_API_KEY,
                           @"id":CMAILCHIP_SUBSCRIBER_ID,
                           @"email[email]":email};
    
    [[self sessionManager] POST:@"" parameters:dict progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        


    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        [self subscribeMailChipEmail:email andUserName:userName completed:completion];
       
        if (completion)
            completion(nil, error);

    }] ;

    
}
- (void)forgotPasswordWithEmail:(NSString *)email completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagForgotPassword parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagForgotPassword andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self forgotPasswordWithEmail:email completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
    
}

- (void)changePasswordWithNewPassword:(NSString *)newPassword andOldPassword:(NSString *)oldPassword completed:(void(^)(id responseObject, NSError *error))completion
{
    
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"password":newPassword,
                           @"old_password":oldPassword};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagChangePassword parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagChangePassword andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self changePasswordWithNewPassword:newPassword andOldPassword:oldPassword completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
}


- (void)verifyEmailAccount:(NSString*)email verificationCode:(NSString *)verificationCode completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email,
                           @"verification_code":verificationCode};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagVerifyEmail parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if ([self isJSONDataValidWithResponse:responseObject])
                 [self saveLoginUserToLocal:responseObject];
             
             completion(responseObject, nil);
             
             
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagVerifyEmail andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self verifyEmailAccount:email verificationCode:verificationCode completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
}

- (void)resendCode:(NSString *)email completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"email":email};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagResendCode parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
             completion(responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagResendCode andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self resendCode:email completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
    
}


- (void)editProfileWithUserName:(NSString *)userName andCountry:(NSNumber*)countryId andSignUpToNewsLetter:(BOOL)isSignUpToNewsLetter completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"username":userName,
                           @"country_id":countryId,
                           @"newsletter":isSignUpToNewsLetter ? @"Yes" : @"No"};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagEditProfile parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             if ([self isJSONDataValidWithResponse:responseObject])
                 [self saveLoginUserToLocal:responseObject];
             
             completion(responseObject, nil);
             
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagEditProfile andError:error andHandler:^(BOOL retry)
          {
              if(retry)
                  [self editProfileWithUserName:userName andCountry:countryId andSignUpToNewsLetter:isSignUpToNewsLetter completed:completion];
              
          }];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)userProfile_completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:CAPITagProfile parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject])
         {
             if([self isJSONDataValidWithResponse:responseObject])
                 [self saveLoginUserToLocal:responseObject];
             
             if(completion)
                 completion(responseObject, nil);
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagProfile andError:error];
         
         if (completion)
             completion(nil, error);
     }];
}

#pragma mark -
#pragma mark - Home

- (NSURLSessionDataTask *)loadCharcterList:(NSString *)search andRange:(NSArray *)range andSeries:(NSArray *)series  andTeam:(NSArray *)team andRarity:(NSArray *)rarity andOffset:(NSInteger)offset completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"search_tearms":search.isBlankValidationPassed ? search : @"",
                           @"range_id":range.count ? [range componentsJoinedByString:@","] : @"" ,
                           @"series_id":series.count ? [series componentsJoinedByString:@","] : @"",
                           @"team_id":team.count ? [team componentsJoinedByString:@","] : @"",
                           @"rarity_id":rarity.count ? [rarity componentsJoinedByString:@","] : @"",
                           @"page":[NSNumber numberWithInteger:offset],
                           @"per_page":CLimit};
    
  return  [[MIAFNetworking sharedInstance] POST:CAPITagCharacterList parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             completion(responseObject, nil);
             
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagCharacterList andError:error];
         if (completion)
             completion(nil, error);
     }];
}

- (NSURLSessionDataTask*)loadCharcterListWithType:(NSInteger)charType  searchString:(NSString *)search andRange:(NSArray *)range andSeries:(NSArray *)series  andTeam:(NSArray *)team andRarity:(NSArray *)rarity andTimestamp:(NSString*)timestamp completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"character_status":[NSNumber numberWithInteger:charType],
                           @"search_tearms":search.isBlankValidationPassed ? search : @"",
                           @"range_id":range.count ? [range componentsJoinedByString:@","] : @"" ,
                           @"series_id":series.count ? [series componentsJoinedByString:@","] : @"",
                           @"team_id":team.count ? [team componentsJoinedByString:@","] : @"",
                           @"rarity_id":rarity.count ? [rarity componentsJoinedByString:@","] : @"",
                           @"timestamp":timestamp,
                           @"per_page":CLimit};
    
    return  [[MIAFNetworking sharedInstance] POST:CAPITagUserCharacterList parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 
                 if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
                 {
                     completion(responseObject, nil);
                     
                 }
                 
             } failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 
                 [self failureWithAPI:CAPITagUserCharacterList andError:error];
                 if (completion)
                     completion(nil, error);
             }];
    
    
}

- (void)loadCharcterDetails:(NSString *)charId completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] GET:[NSString stringWithFormat:@"%@%@",CAPITagCharacterDetails,charId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
         {
             completion(responseObject, nil);
             
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         [self failureWithAPI:CAPITagCharacterDetails andError:error];
         if (completion)
             completion(nil, error);
     }];
}


- (void)changeCharacterStatusWithCharId:(NSString *)charId withStatus:(NSInteger)status completed:(void (^)(id responseObject, NSError *error))completion
{
    NSDictionary *dict = @{@"character_status":[NSNumber numberWithInteger:status],
                           @"character_id":charId};
    
      [[MIAFNetworking sharedInstance] POST:CAPITagSelectCharacter parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
             {
                 if([self isSuccessHTTPStatusWithDataTask:task withResponse:responseObject] && completion)
                 {
                     if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                     {
                         appDelegate.loginUser.total_want = [[[responseObject valueForKey:CJsonData] valueForKey:@"total_want"] integerValue];;
                         appDelegate.loginUser.total_own = [[[responseObject valueForKey:CJsonData] valueForKey:@"total_own"] integerValue];
                          [[Store sharedInstance].mainManagedObjectContext save];
                     }
                     completion(responseObject, nil);
                     
                 }
                 
             } failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 
                 [self failureWithAPI:CAPITagSelectCharacter andError:error];
                 if (completion)
                     completion(nil, error);
             }];
}

#pragma mark -
#pragma mark - Save In Local

- (void)saveLoginUserToLocal:(id)responseObject
{
    appDelegate.loginUser = [self userWithDictionary:[responseObject valueForKey:CJsonData]];
    
    
    if (appDelegate.loginUser)
    {
        NSDictionary *dictMeta = [responseObject valueForKey:CJsonMeta];
        if ([dictMeta valueForKey:@"token"])
        {
            [CUserDefaults setObject:[dictMeta stringValueForJSON:@"token"] forKey:UserDefaultLoginToken];
            [CUserDefaults synchronize];
        }
        
        [UIApplication setUserId:[[responseObject valueForKey:CJsonData] stringValueForJSON:@"id"]];
    }
}


#pragma mark -
#pragma mark - Helper Method

- (TBLUser *)userWithDictionary:(NSDictionary *)dictUser
{
    TBLUser *user;
    
    if (dictUser)
    {
        user = (TBLUser *)[TBLUser findOrCreate:@{@"user_id":[dictUser stringValueForJSON:@"id"]}];
        
        //....UserInfo
        user.user_name                  = [dictUser stringValueForJSON:@"username"];
        user.email                      = [dictUser stringValueForJSON:@"email"];
        user.country_id                 = [dictUser integerForKey:@"country_id"];
        user.parent_email               = [dictUser stringValueForJSON:@"parent_email"];
        user.dob                        = [dictUser stringValueForJSON:@"date_of_birth"];
        user.newsletter                 = [[dictUser stringValueForJSON:@"newsletter"] isEqualToString:@"Yes"];
        user.total_own                  = [dictUser integerForKey:@"total_own"];
        user.total_want                 = [dictUser integerForKey:@"total_want"];
        user.status                     = [dictUser integerForKey:@"status_id"];
        
        
        [[Store sharedInstance].mainManagedObjectContext save];
    }
    
    return user;
}

@end

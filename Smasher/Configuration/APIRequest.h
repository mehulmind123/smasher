//
//  APIRequest.h
//  Smasher
//
//  Created by Mac-00014 on 04/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

//Local server
//#define BASEURL         @"http://192.168.1.31/smasher/api/"

//VPN server
//#define BASEURL         @"http://180.211.104.102/smasher/api/"

//Magento server
//#define BASEURL         @"http://www.magentodevelopments.com/smasher/api/"

//Live server
#define BASEURL         @"https://mycollection.smashersworld.zuru.com/api/"




//********** Request, Response Constant
#define CVersion    @"1"
#define CLimit      @20

#define CJsonStatus             @"status"
#define CJsonUserStatus         @"user_status"
#define CJsonStatusCode         @"status_code"
#define CJsonMessage            @"message"
#define CJsonData               @"data"
#define CJsonTimestamp          @"timestamp"
#define CJsonMeta               @"meta"
#define CJsonPagination         @"pagination"


#define CStatusZero             0
#define CStatusOne              1
#define CStatusTwo              2
#define CStatusThree            3
#define CStatusFour             4
#define CStatusFive             5
#define CStatusNine             9
#define CStatusTen              10
#define CStatusTwoHundred       200  // OK
#define CStatusTwoHundredOne    201  // Created
#define CStatusFourHundred      400  // Error
#define CStatusFourHundredOne   401  // Unauthorized
#define CStatusFourHundredTwo   402  // User Email Not Verified
#define CStatusFourHundredTen   503  // Under Maintanance

//...
#define CCharStatusNone             0
#define CCharStatusOwn              1
#define CCharStatusWant             2


//********** API Tag
#define CAPITagCountries                                @"countries"
#define CAPITagCMSDetails                               @"cms"
#define CAPITagGeneralDetails                           @"general"

#define CAPITagSignIn                                   @"login"
#define CAPITagSignup                                   @"signup"
#define CAPITagProfile                                  @"user-details"
#define CAPITagForgotPassword                           @"forgot-password"
#define CAPITagChangePassword                           @"change-password"
#define CAPITagEditProfile                              @"change-profile"


#define CAPITagCharacterList                            @"character"
#define CAPITagUserCharacterList                        @"user-character"
#define CAPITagCharacterDetails                         @"character/details?id="
#define CAPITagSelectCharacter                          @"select-character"
#define CAPITagVisitores                                @"visitor"



//...

#define CAPITagTopics                               @"topics"
#define CAPITagAddTopics                            @"topics/add"

#define CAPITagResendCode                           @"resend-code"
#define CAPITagVerifyEmail                          @"verify-email"

#define CAPITagVerifyOTP                            @"verify-otp"
#define CAPITagResetPassword                        @"reset-password"


#define CAPITagAddAccessCode                        @"access-code/add"
#define CAPITagDeleteAccessCode                      @"access-code/delete"
#define CAPITagAccessCode                           @"access-code"

#define CAPITagBlockedCustomer                      @"blocked-customer"

#define CAPITagAddDeviceToken                       @"device-token"
#define CAPITagDeleteDeviceToken                    @"delete-device-token"

#define CAPITagChangeAllowNotification              @"update-notification"

#define CAPITagAllSurvey                            @"all-survey"
#define CAPITagMySurvey                             @"my-survey"
#define CAPITagNewSurvey                            @"suggested-surveys"
#define CAPITagAddComment                           @"comment/add"
#define CAPITagSurveyComments                       @"survey-comments"
#define CAPITagDeleteComments                       @"comment/delete"

#define CAPITagAddBlockCustomer                     @"blocked-customer/add"
#define CAPITagBlockCustomerList                     @"blocked-customer"
#define CAPITagBlockUnblockCustomer                 @"customer-block-unblock"
#define CAPITagAddTopicsFromSurvey                  @"survey/topic/add"
#define CAPITagSubmitSurvey                         @"survey/add"
#define CAPITagSurveyDetails                         @"survey/detail"
#define CAPITagSurveyResults                         @"survey-result"
#define CAPITagUserRespondedCount                    @"user-responded-count"

@interface APIRequest : NSObject

+ (id)request;

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error;

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler;

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task;

- (BOOL)isJSONDataValidWithResponse:(id)response;

- (BOOL)isJSONStatusValidWithResponse:(id)response;

- (BOOL)isUserNotVerifiedWithResponse:(id)response;

- (BOOL)checkResponseStatus:(id)response;

- (NSInteger)paginationWithResponse:(id)response;

#pragma mark -
#pragma mark - General
- (void)loadGeneralData_completed:(void (^)(id responseObject, NSError *error))completion;

- (void)loadCMS:(NSInteger)type   completed:(void (^)(id responseObject, NSError *error))completion;

- (void)loadCountryList_completed:(void (^)(id responseObject, NSError *error))completion;

- (void)visitores_completed:(void (^)(id responseObject, NSError *error))completion;


#pragma mark -
#pragma mark - LRF

- (void)signInWithEmail:(NSString *)email andPassword:(NSString *)password completed:(void (^)(id responseObject, NSError *error))completion;

- (void)signupWithEmail:(NSString *)email andPassword:(NSString *)password andUserName:(NSString *)userName andDOB:(NSString*)dob andParentEmail:(NSString*)parentEmail andCountry:(NSNumber*)countryId andSignUpToNewsLetter:(BOOL)isSignUpToNewsLetter completed:(void (^)(id responseObject, NSError *error))completion;

- (void)subscribeMailChipEmail:(NSString *)email andUserName:(NSString *)userName completed:(void (^)(id responseObject, NSError *error))completion;


- (void)forgotPasswordWithEmail:(NSString *)email completed:(void (^)(id responseObject, NSError *error))completion;

- (void)changePasswordWithNewPassword:(NSString *)newPassword andOldPassword:(NSString *)oldPassword completed:(void(^)(id responseObject, NSError *error))completion;


- (void)verifyEmailAccount:(NSString*)email verificationCode:(NSString *)verificationCode completed:(void(^)(id responseObject, NSError *error))completion;


- (void)resendCode:(NSString *)email completed:(void(^)(id responseObject, NSError *error))completion;

- (void)editProfileWithUserName:(NSString *)userName andCountry:(NSNumber*)countryId andSignUpToNewsLetter:(BOOL)isSignUpToNewsLetter completed:(void (^)(id responseObject, NSError *error))completion;


- (void)userProfile_completed:(void (^)(id responseObject, NSError *error))completion;

#pragma mark -
#pragma mark - Home

- (NSURLSessionDataTask*)loadCharcterList:(NSString *)search andRange:(NSArray *)range andSeries:(NSArray *)series  andTeam:(NSArray *)team andRarity:(NSArray *)rarity andOffset:(NSInteger)offset completed:(void (^)(id responseObject, NSError *error))completion;

- (NSURLSessionDataTask*)loadCharcterListWithType:(NSInteger)charType  searchString:(NSString *)search andRange:(NSArray *)range andSeries:(NSArray *)series  andTeam:(NSArray *)team andRarity:(NSArray *)rarity andTimestamp:(NSString*)timestamp completed:(void (^)(id responseObject, NSError *error))completion;

- (void)changeCharacterStatusWithCharId:(NSString *)charId withStatus:(NSInteger)status completed:(void (^)(id responseObject, NSError *error))completion;

- (void)loadCharcterDetails:(NSString *)charId completed:(void (^)(id responseObject, NSError *error))completion;

#pragma mark -
#pragma mark - Save In Local

- (void)saveLoginUserToLocal:(id)responseObject;
@end

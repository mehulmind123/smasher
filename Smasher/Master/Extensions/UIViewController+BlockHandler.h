//
//  UIViewController+BlockHandler.h
//  MI API Example
//
//  Created by mac-0001 on 20/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Master.h"

@interface UIViewController (BlockHandler)

-(void)setBlock:(MIIdResultBlock)block;

-(MIIdResultBlock)block;

@end

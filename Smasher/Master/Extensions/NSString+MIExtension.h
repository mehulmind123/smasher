//
//  NSString+MIExtension.h
//  VLB
//
//  Created by mac-0001 on 7/16/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MIExtension)


-(NSString *)bundlePath;
-(NSURL *)bundleURLForImage;

-(BOOL)isServerURL;
-(BOOL)isLocalURL;

- (NSURL *)urlForString;

- (void)deleteFileFromPath;


-(NSString *)stringWithFirstLetterCapital;

-(BOOL)isValidUrl;
-(BOOL)isValidEmailAddress;
-(BOOL)isValidPhoneNumber;
-(BOOL)isBlankValidationPassed;
-(BOOL)isAlphaNumericValidationPassed;
-(BOOL)isNonZeroNumber;
-(BOOL)isValidName:(NSString *)text;
- (NSString*)stringByTruncatingAtString:(NSString *)string toWidth:(CGFloat)width withFont:(UIFont *)font;

@end

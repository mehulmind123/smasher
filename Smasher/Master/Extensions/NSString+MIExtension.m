//
//  NSString+MIExtension.m
//  VLB
//
//  Created by mac-0001 on 7/16/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "NSString+MIExtension.h"

#define ellipsis @"…"
@implementation NSString (MIExtension)

#pragma mark -- Path and URL


- (NSString *)bundlePath
{
    return [[NSBundle mainBundle] pathForResource:self ofType:nil];
}

-(NSURL *)bundleURLForImage
{
    return [[self bundlePath] urlForString];
}

-(BOOL)isServerURL
{
    if ([self rangeOfString:@"http:"].location==NSNotFound && [self rangeOfString:@"https:"].location==NSNotFound)
        return NO;
    else
        return YES;
}

-(BOOL)isLocalURL
{
    if ([self isServerURL])
        return NO;
    else
        return YES;
}

- (NSURL *)urlForString
{
    if ([self isServerURL])
        return [NSURL URLWithString:self];
    else
        return [NSURL fileURLWithPath:self];
}

- (void)deleteFileFromPath
{
    NSError *error = nil;
    if(![[NSFileManager defaultManager] removeItemAtPath:self error:&error])
    {
        NSLog(@"File Delete failed:%@",error);
    }
    else
    {
        NSLog(@"File removed: %@",self);
    }
}



#pragma mark -- Validations


-(BOOL)isValidEmailAddress
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[+\\w\\.\\-']+@[a-zA-Z0-9-]+(\\.[a-zA-Z]{2,})+$" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSInteger num_matches = [regex numberOfMatchesInString:self options:0 range:NSMakeRange(0, self.length)];
    
    if (num_matches == 1)
        return YES;
    else
        return NO;
}

-(BOOL)isValidPhoneNumber
{
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^((\\+)|(00))[0-9]{6,14}$" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSInteger num_matches = [regex numberOfMatchesInString:self options:0 range:NSMakeRange(0, self.length)];
    
    if (num_matches == 1)
        return YES;
    else
        return NO;
}

-(BOOL)isBlankValidationPassed
{
    if ([self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length>0)
        return YES;
    
    return NO;
}

-(BOOL)isAlphaNumericValidationPassed
{
//    NSError *error = NULL;
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^(?=.*[a-z])(?=.*\\d)[a-z\\d]*$"
//                                                                           options:NSRegularExpressionCaseInsensitive
//                                                                             error:&error];
//    NSArray *matches = [regex matchesInString:self
//                                      options:0
//                                        range:NSMakeRange(0, self.length)];
//    
//    if([matches count] > 0)
//        return true;
//    else
//        return false;
    if (!self.isBlankValidationPassed) 
        return NO;
        
    NSCharacterSet *alphanumericSet = [NSCharacterSet alphanumericCharacterSet];
    return [[self stringByTrimmingCharactersInSet:alphanumericSet] isEqualToString:@""];
}

-(BOOL)isNonZeroNumber
{
    if ([self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length>0)
    {
        if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue]>0)
            return YES;
        else
            return NO;
    }
    
    return NO;
}

- (BOOL)isValidUrl
{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:self];
}

-(BOOL)isValidName:(NSString *)text {
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[a-zA-Z0-9._]+$" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSInteger num_matches = [regex numberOfMatchesInString:text options:0 range:NSMakeRange(0, text.length)];
    
    if (num_matches == 1)
        return YES;
    else
        return NO;
}
#pragma mark -- Other

-(NSString *)stringWithFirstLetterCapital
{
    return [NSString stringWithFormat:@"%@%@",[[self substringToIndex:1] uppercaseString],[self substringFromIndex:1]];
}

- (NSString*)stringByTruncatingAtString:(NSString *)string toWidth:(CGFloat)width withFont:(UIFont *)font
{
    // If the string is already short enough, or
    // if the 'truncation location' string doesn't exist
    // go ahead and pass the string back unmodified.
    if ([self sizeWithAttributes:@{NSFontAttributeName:font}].width < width ||
        [self rangeOfString:string].location == NSNotFound)
        return self;
    
    // Create copy that will be the returned result
    NSMutableString *truncatedString = [self mutableCopy];
    
    // Accommodate for ellipsis we'll tack on the beginning
    width -= [ellipsis sizeWithAttributes:@{NSFontAttributeName:font}].width;
    
    // Get range of the passed string. Note that this only works to the first instance found,
    // so if there are multiple, you need to modify your solution
    NSRange range = [truncatedString rangeOfString:string];
    range.length = 1;
    
    while([truncatedString sizeWithAttributes:@{NSFontAttributeName:font}].width > width
          && range.location > 0)
    {
        range.location -= 1;
        [truncatedString deleteCharactersInRange:range];
    }
    
    // Append ellipsis
    range.length = 0;
    [truncatedString replaceCharactersInRange:range withString:ellipsis];
    
    return truncatedString;
}
@end

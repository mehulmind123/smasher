//
//  UIButton+EventHandler.m
//  MI API Example
//
//  Created by mac-0001 on 12/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UIButton+EventHandler.h"

#import "Master.h"
#import "NSObject+NewProperty.h"


static NSString *const BUTTONCALLBACKHANDLER = @"buttonHandler";


@implementation UIButton (EventHandler)

#pragma mark - Touch Up Inside Handler


- (void)touchUpInsideClicked:(TouchUpInsideHandler)clicked
{
    [self setObject:clicked forKey:BUTTONCALLBACKHANDLER];
    [self addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)touchUpInsideEventHandler:(TouchUpInsideEventHandler)handler
{
    [self setObject:handler forKey:BUTTONCALLBACKHANDLER];
    [self addTarget:self action:@selector(touchUpInside:forEvent:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)touchUpInsideViewHandler:(TouchUpInsideViewHandler)handler
{
    [self setObject:handler forKey:BUTTONCALLBACKHANDLER];
    [self addTarget:self action:@selector(touchUpInsideView:forEvent:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark -
#pragma mark - Clicked

- (void)touchUpInside:(UIButton *)sender
{
    TouchUpInsideHandler touchUpInsideHandler = [self objectForKey:BUTTONCALLBACKHANDLER];
    
    if (touchUpInsideHandler)
        touchUpInsideHandler();
}

- (void)touchUpInside:(UIButton *)sender forEvent:(UIEvent *)event
{
    TouchUpInsideEventHandler touchUpInsideEventHandler = [self objectForKey:BUTTONCALLBACKHANDLER];
    
    if (touchUpInsideEventHandler)
        touchUpInsideEventHandler(sender, event);
}

- (void)touchUpInsideView:(UIButton *)sender forEvent:(UIEvent *)event
{
    TouchUpInsideViewHandler touchUpInsideViewHandler = [self objectForKey:BUTTONCALLBACKHANDLER];
    
    if (touchUpInsideViewHandler)
    {
        CGRect imgRect = sender.imageView.frame;
        imgRect.origin = CGPointMake(0, 0);
        imgRect.size.height = CViewHeight(sender);
        
        CGRect lblRect = sender.titleLabel.frame;
        lblRect.origin.y = 0;
        lblRect.size.height = CViewHeight(sender);
        
        UITouch *touch = [[event touchesForView:sender] anyObject];
        CGPoint location = [touch locationInView:sender];
        
        if (CGRectContainsPoint(imgRect, location))
            touchUpInsideViewHandler(sender.imageView);
        else if (CGRectContainsPoint(lblRect, location))
            touchUpInsideViewHandler(sender.titleLabel);
        else
            touchUpInsideViewHandler(sender);
    }
}

#pragma mark -
#pragma mark - Underline label 

- (void)setUnderlineHeight:(CGFloat)height andColor:(UIColor *)color
{
    [self layoutIfNeeded];
    CGRect uFrame = self.titleLabel.frame;
    uFrame.origin.y = uFrame.origin.y + uFrame.size.height-2;
    uFrame.size.height = height;
    
    UIView *underline = [self viewWithTag:1000];
    if (!underline)
    {
        underline = [[UIView alloc] init];
        underline.backgroundColor = color;
        underline.tag = 1000;
        [self addSubview:underline];
    }
    
    [underline setFrame:uFrame];
}

@end

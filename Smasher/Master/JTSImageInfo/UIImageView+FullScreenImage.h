//
//  UIImageView+FullScreenImage.h
//  Master
//
//  Created by mac-0001 on 11/03/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (FullScreenImage)

-(void)enableFullScreenImage;

@end

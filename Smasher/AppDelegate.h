//
//  AppDelegate.h
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "TBLUser+CoreDataProperties.h"
#import "TBLUser+CoreDataClass.h"
#import <UserNotifications/UserNotifications.h>
#import "BasicAppDelegate.h"
#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"

@interface AppDelegate : BasicAppDelegate <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) LGSideMenuController *sidePanelViewController;
@property (assign, nonatomic) BOOL isLogin;
@property (assign, nonatomic) BOOL isSpanish;
@property (nonatomic, strong) TBLUser *loginUser;

//....General
- (void)signinUser;
- (void)logoutUser;
- (void)logoutUserFromSideMenu;
- (void)popToHomeViewController;
- (void)initiateLoadingScreen;

- (void)showLoginRequirePopUp:(UIViewController*)viewController;
- (void)showSelectDOBPopUp:(UIViewController*)viewController;
- (NSURL *)imageURL:(NSString *)url withSize:(CGSize)size;

- (BOOL)sidePanelViewControllerInitiate;

@end


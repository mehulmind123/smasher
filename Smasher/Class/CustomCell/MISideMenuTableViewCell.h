//
//  MISideMenuTableViewCell.h
//  Smasher
//
//  Created by Mac-00014 on 27/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISideMenuTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@end

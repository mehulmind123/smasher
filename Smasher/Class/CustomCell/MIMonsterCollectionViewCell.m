//
//  MIMonsterCollectionViewCell.m
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIMonsterCollectionViewCell.h"

@implementation MIMonsterCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_lblcharCode setAttributedText:[self setAttributeText:@"000" value:@"xxx"]];
    
    self.viewContent.layer.cornerRadius = 5;
    self.viewContent.layer.shadowColor = ColorBlack_000000.CGColor;
    self.viewContent.layer.shadowOffset = CGSizeZero;
    self.viewContent.layer.shadowRadius = 3.0f;
    self.viewContent.layer.shadowOpacity = 0.5f;
}

- (void)configureData:(NSDictionary*)dict indexPath:(NSIndexPath*)indexPath
{
    if(dict)
    {
       // _imgVChar.image = [UIImage imageNamed:(indexPath.row % 2) == 0 ? @"Character1.png" : @"Character2.png"];
        [_lblCharName setText:[dict stringValueForJSON:@"character_name"]];
        [_lblcharCode setAttributedText:[self setAttributeText:[dict stringValueForJSON:@"code"] value:[dict stringValueForJSON:@"value"]]];
        [_imgVChar sd_setImageWithURL:[appDelegate imageURL:[dict stringValueForJSON:@"image"] withSize:_imgVChar.frame.size]];
        
        
        [self switchSelection:[dict integerForKey:@"character_status"]];
        
}
    
}
- (IBAction)btnSwitchClicked:(UIButton*)sender
{
      
    if(sender.selected)
    {
        sender.selected = NO;
        return;
    }
    self.btnIOwn.selected =
    self.btnIWant.selected = NO;

    [sender setSelected:YES];
    [sender setTitle:@"" forState:UIControlStateSelected];
    
}

- (void)switchSelection:(NSInteger)status
{
    switch (status)
    {
        case CCharStatusOwn:
        {
            self.btnIOwn.selected = YES;
            [self.btnIOwn setTitle:@"" forState:UIControlStateSelected];
            self.btnIWant.selected = NO;
            break;
        }
        case CCharStatusWant:
        {
            self.btnIOwn.selected = NO;
            self.btnIWant.selected = YES;
            [self.btnIWant setTitle:@"" forState:UIControlStateSelected];
            break;
        }

        default:
        {
            self.btnIOwn.selected =
            self.btnIWant.selected = NO;
            break;
        }
            
    }
}

- (NSAttributedString*)setAttributeText:(NSString*)code value:(NSString*)value
{
    NSString *string = [NSString stringWithFormat:@"# %@ I  S.P %@",code,value];
    //....Attributed String
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?12:(Is_iPhone_6_PLUS)?16:(IS_IPAD ? 18 : 14);
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttributes:@{NSFontAttributeName : CFontGroBold(fontSize) }range:NSMakeRange(0, string.length)];
    [attString addAttribute:NSFontAttributeName value:CFontRobotoBold(fontSize) range:[string rangeOfString:@"#"]];
    [attString addAttributes:@{NSFontAttributeName : CFontRobotoBold(fontSize),
                               NSForegroundColorAttributeName : CRGB(100, 100, 100)
                               } range:[string rangeOfString:@"I"]];
    return attString;
}
@end

//
//  MIFilterTableViewCell.h
//  Smasher
//
//  Created by Mac-00014 on 27/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIFilterTableViewCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UILabel *lblTitle;

@property (nonatomic, weak)IBOutlet UIButton *btnClose;

@property (nonatomic, weak)IBOutlet UIView *vWContent;

- (void)configureSelected:(BOOL)isSelected;
@end

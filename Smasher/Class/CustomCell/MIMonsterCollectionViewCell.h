//
//  MIMonsterCollectionViewCell.h
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIMonsterCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIButton *btnIOwn;
@property (nonatomic, strong) IBOutlet UIButton *btnIWant;

@property (nonatomic, strong) IBOutlet UIImageView *imgVChar;

@property (nonatomic, strong) IBOutlet UIView *viewContent;

@property (nonatomic, strong) IBOutlet UILabel *lblCharName;
@property (nonatomic, strong) IBOutlet UILabel *lblcharCode;

- (void)configureData:(NSDictionary*)dict indexPath:(NSIndexPath*)indexPath;

- (IBAction)btnSwitchClicked:(UIButton*)sender;

- (NSAttributedString*)setAttributeText:(NSString*)code value:(NSString*)value;

@end

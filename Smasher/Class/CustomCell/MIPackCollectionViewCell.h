//
//  MIPackCollectionViewCell.h
//  Smasher
//
//  Created by Mac-00014 on 28/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CTextPadding                20
#define CMinimumLineSpacing         6
#define CMinimumInteritemSpacing    6

@interface MIPackCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblPackName;
@end

//
//  MIPackCollectionViewCell.m
//  Smasher
//
//  Created by Mac-00014 on 28/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIPackCollectionViewCell.h"

@implementation MIPackCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.layer setCornerRadius:CViewHeight(self)/2];
    [self.layer setBorderWidth:1];
    [self.layer setBorderColor:ColorBlue_6BCBF3.CGColor];
}


@end

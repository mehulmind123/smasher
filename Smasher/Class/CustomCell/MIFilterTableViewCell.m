//
//  MIFilterTableViewCell.m
//  Smasher
//
//  Created by Mac-00014 on 27/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIFilterTableViewCell.h"

@implementation MIFilterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configureSelected:(BOOL)isSelected
{
    if(isSelected)
    {
        [_vWContent setBackgroundColor:ColorBlue_0E6EB8];
        [_lblTitle setTextColor:ColorWhite_FFFFFF];
        [_btnClose setHidden:NO];
    }
    else
    {
        [_vWContent setBackgroundColor:ColorGray_F0F0F0];
        [_lblTitle setTextColor:ColorBlue_0E6EB8];
        [_btnClose setHidden:YES];

    }
}
@end

//
//  MIForgotPasswordViewController.h
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIForgotPasswordViewController : MIParentViewController
{
    IBOutlet MIGenericLabel *lblDetail;
    IBOutlet UITextField *txtEmail;
}
@end

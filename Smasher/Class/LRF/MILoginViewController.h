//
//  MILoginViewController.h
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"
#import "TTTAttributedLabel.h"

@interface MILoginViewController : MIParentViewController
{
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UILabel *lblTitle;
    IBOutlet UIButton *btnAgePermission;
    IBOutlet TTTAttributedLabel *lblPpAndTc;
    IBOutlet UIButton *btnPpAndTc;
}
@end

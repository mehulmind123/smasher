//
//  MIRegisterViewController.h
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"
#import "TTTAttributedLabel.h"

@interface MIRegisterViewController : MIParentViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>
{
    IBOutlet UITextField *txtName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtParentsEmail;
    IBOutlet UITextField *txtPassowrd;
    IBOutlet UITextField *txtConfirmPassowrd;
    IBOutlet UITextField *txtCountry;
    IBOutlet UIButton *btnNewsLetter;
    
    IBOutlet UIButton *btnPrivacyPolicy;
    IBOutlet UIButton *btnChildrenPrivacyPolicy;
    IBOutlet UIButton *btnAgePermission;
    IBOutlet UIButton *btnPpAndTc;
    IBOutlet UILabel *lblView;
    IBOutlet UILabel *lblViewChildren;
    IBOutlet TTTAttributedLabel *lblPpAndTc;
    IBOutlet NSLayoutConstraint *cnParentTop;
}
@property (nonatomic, assign) BOOL isFromHome;
@end

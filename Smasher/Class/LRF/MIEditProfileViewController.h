//
//  MIEditProfileViewController.h
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIEditProfileViewController : MIParentViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>
{
    IBOutlet UITextField *txtName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtDOB;
    IBOutlet UITextField *txtCountry;
    
    IBOutlet UIButton *btnNewsLetter;
    
    IBOutlet UIView *vWFooter;
}
@end

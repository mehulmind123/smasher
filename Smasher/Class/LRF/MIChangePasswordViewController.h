//
//  MIChangePasswordViewController.h
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIChangePasswordViewController : MIParentViewController
{
    IBOutlet UITextField *txtOldPassword;
    IBOutlet UITextField *txtNewPassword;
    IBOutlet UITextField *txtConfirmNewPassword;
    
    IBOutlet UIView *vWFooter;
}
@end

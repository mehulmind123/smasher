//
//  MIRegisterViewController.m
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIRegisterViewController.h"
#import "MIWebViewViewController.h"
#import "MIInAppWebViewController.h"
#import "MILoginViewController.h"
#import "TTTAttributedLabel.h"

@interface MIRegisterViewController ()<TTTAttributedLabelDelegate>
{
    NSString *strBirthDate;
    NSArray *arrCountryList;
    NSNumber *countryId;
    UIPickerView *myPickerView;
    
    BOOL isChild;
}
@end

@implementation MIRegisterViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{  
   [UIApplication sharedApplication].statusBarHidden = NO;
    isChild = YES;
    
    [btnChildrenPrivacyPolicy setTitle: appDelegate.isSpanish ? CLocalize(@"Children's Privacy Policy") : @"Children\u2019s Privacy Policy" forState:UIControlStateNormal];
    arrCountryList = [CUserDefaults valueForKey:UserDefaultCountryList];
    

//    if (CDeviceHeight > 568 )
//    {
//        [txtCountry setPickerData:[arrCountryList valueForKeyPath:@"name"] update:^(NSString *text, NSInteger row, NSInteger component)
//         {
//
//             countryId = arrCountryList[row][@"id"];
//         }];
//
//        [[APIRequest request] loadCountryList_completed:^(id responseObject, NSError *error)
//         {
//             arrCountryList = [CUserDefaults valueForKey:UserDefaultCountryList];
//
//             [txtCountry setPickerData:[arrCountryList valueForKeyPath:@"name"] update:^(NSString *text, NSInteger row, NSInteger component)
//              {
//
//                  countryId = arrCountryList[row][@"id"];
//              }];
//
//         }];
//    }
//    else
//    {
        [[APIRequest request] loadCountryList_completed:^(id responseObject, NSError *error)
         {
             arrCountryList = [CUserDefaults valueForKey:UserDefaultCountryList];
             
         }];
        txtCountry.delegate = self;
  //  }
    
    
    //...
    self.title = CLocalize(@"Create an Account");
    
    if (IS_IPAD)
    {
        [txtName setLeftImage:[UIImage imageNamed:@"name"] withSize:CGSizeMake(70, 35)];
        [txtEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(70, 28)];
        [txtParentsEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(70, 28)];
        [txtPassowrd setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(70, 35)];
        [txtConfirmPassowrd setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(70, 35)];
        [txtCountry setLeftImage:[UIImage imageNamed:@"country"] withSize:CGSizeMake(70, 35)];
        [txtCountry setRightImage:[UIImage imageNamed:@"dropdown_blue"] withSize:CGSizeMake(50, 15)];
    }
    else
    {
        
        [txtName setLeftImage:[UIImage imageNamed:@"name"] withSize:CGSizeMake(50, 25)];
        [txtEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(50, 20)];
        [txtParentsEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(50, 20)];
        [txtPassowrd setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(50, 25)];
        [txtConfirmPassowrd setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(50, 25)];
        [txtCountry setLeftImage:[UIImage imageNamed:@"country"] withSize:CGSizeMake(50, 25)];
        [txtCountry setRightImage:[UIImage imageNamed:@"dropdown_blue"] withSize:CGSizeMake(28, 8)];
    }
    
   
    
    //...
    lblViewChildren.layer.shadowColor =
    lblView.layer.shadowColor =
    btnChildrenPrivacyPolicy.titleLabel.layer.shadowColor =
    btnPrivacyPolicy.titleLabel.layer.shadowColor = ColorGray_E5E5E5.CGColor;
    lblViewChildren.layer.shadowRadius =
    lblView.layer.shadowRadius =
    btnChildrenPrivacyPolicy.titleLabel.layer.shadowRadius =
    btnPrivacyPolicy.titleLabel.layer.shadowRadius = 0.3;
    lblViewChildren.layer.shadowOpacity =
    lblView.layer.shadowOpacity =
    btnChildrenPrivacyPolicy.titleLabel.layer.shadowOpacity =
    btnPrivacyPolicy.titleLabel.layer.shadowOpacity = 1.0;
    lblViewChildren.layer.shadowOffset =
    lblView.layer.shadowOffset =
    btnChildrenPrivacyPolicy.titleLabel.layer.shadowOffset =
    btnPrivacyPolicy.titleLabel.layer.shadowOffset = CGSizeMake(2,2);
    lblViewChildren.layer.masksToBounds =
    lblView.layer.masksToBounds =
    btnChildrenPrivacyPolicy.titleLabel.layer.masksToBounds =
    btnPrivacyPolicy.titleLabel.layer.masksToBounds = false;
    
    //...
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    strBirthDate = [dateFormatter stringFromDate:self.iObject];
    NSDate *birthDate = [dateFormatter dateFromString:strBirthDate];
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]                                                components:NSCalendarUnitYear fromDate:birthDate toDate:now options:0];
    
   NSInteger age = [ageComponents year];
    
    if(age >= CMinimumAge)
    {
        isChild = NO;
        [txtParentsEmail hideByHeight:YES];
        cnParentTop.constant = 0;
    }
    
    [txtParentsEmail setPlaceholder: appDelegate.isSpanish ? CLocalize(@"Parent's Email") : [NSString stringWithFormat:@"Parent%@s Email",@"\u2019"]];
    [txtParentsEmail setPlaceHolderColor:ColorBlue_1493B3];
    
    [self setupAttributedText];
    
}

-(void)addPickerView
{
    
    myPickerView = [[UIPickerView alloc]init];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    txtCountry.inputView = myPickerView;
    UIButton *btnDone = [[UIButton alloc] initWithFrame:CGRectMake(CScreenWidth - (IS_IPAD ? 80:60), 0, (IS_IPAD ? 80:50), 50)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone.titleLabel setFont:[UIFont boldSystemFontOfSize:(IS_IPAD ? 21 : 17)]];
    [btnDone setTitleColor:CRGB(0, 0, 0) forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(btnDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lblPlaceholer = [[UILabel alloc] initWithFrame:CGRectMake(60, 0,CScreenWidth - 120 , 50 )];
    lblPlaceholer.text = txtCountry.placeholder;
    lblPlaceholer.textColor = CRGB(171, 171, 171);
    lblPlaceholer.font = [UIFont systemFontOfSize:(IS_IPAD ? 18 : 14)];
    lblPlaceholer.textAlignment = NSTextAlignmentCenter;
    UIView *vWAccessor = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-
                                                                  myPickerView.frame.size.height - 50, CScreenWidth, 50)];
    [vWAccessor addSubview:btnDone];
    [vWAccessor addSubview:lblPlaceholer];
    vWAccessor.backgroundColor = CRGB(247, 247, 247);
    txtCountry.inputAccessoryView = vWAccessor;
    
    if ([txtCountry.text isBlankValidationPassed])
    {
        [myPickerView reloadAllComponents];
        [myPickerView selectRow:[[arrCountryList valueForKeyPath:@"name"] indexOfObject:txtCountry.text] inComponent:0 animated:false];
    }
}



-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self addPickerView];
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return arrCountryList.count;
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    
    [txtCountry setText:[[arrCountryList objectAtIndex:row] stringValueForJSON:@"name"] ];
    countryId = arrCountryList[row][@"id"];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
    return [[arrCountryList objectAtIndex:row] stringValueForJSON:@"name"];
}

#pragma mark Function
-(void)popToLoginViewController:(BOOL)isFromHome
{
    if (isFromHome) {
        MILoginViewController *loginVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
        [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:loginVC]];
        [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:nil];
    }else
    {
       [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark -
#pragma mark - Action Event
-(IBAction)btnDoneClicked:(UIButton*)sender
{
    [myPickerView removeFromSuperview];
    myPickerView = nil;
    [txtCountry resignFirstResponder ];
}
-(IBAction)btnSubmitClicked:(UIButton*)sender
{
    [self resignKeyboard];

    if(![txtName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageBlankUserName) onView:self];
    
    else if(![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageBlankEmail) onView:self];
    
    else if(![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageValidEmail) onView:self];
    
    else if(![txtParentsEmail.text isBlankValidationPassed] && isChild)
        [CustomAlertView iOSAlert:@"" withMessage: appDelegate.isSpanish ? CLocalize(CMessageBlankParentEmail) : @"Please enter your parent\u2019s email address." onView:self];
        
    else if(![txtParentsEmail.text isValidEmailAddress] && isChild)
            [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageValidEmail) onView:self];
    
    else if([txtParentsEmail.text isEqualToString:txtEmail.text] && isChild)
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageEmailAndParentEmailSame) onView:self];
    
    else if(![txtPassowrd.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageEnterPassword) onView:self];
    
    else if(![txtPassowrd.text isAlphaNumericValidationPassed] || txtPassowrd.text.length < 6)
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageAlphaNumbricPassword) onView:self];

    else if(![txtConfirmPassowrd.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageEnterConfirmPassword) onView:self];
    
    else if(![txtConfirmPassowrd.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageEnterConfirmPassword) onView:self];
    
    else if(![txtConfirmPassowrd.text  isEqualToString:txtPassowrd.text])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageConfirmPasswordNotMatch) onView:self];
    
    else if(![txtCountry.text  isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageSelectCountry) onView:self];
    
    else if(!btnAgePermission.isSelected)
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageAgePermission) onView:self];
    
    else if(!btnPpAndTc.isSelected)
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageTermsOfUse) onView:self];
    
    else
    {
        [[APIRequest request] signupWithEmail:txtEmail.text andPassword:txtPassowrd.text andUserName:txtName.text andDOB:strBirthDate andParentEmail:txtParentsEmail.text andCountry:countryId andSignUpToNewsLetter:btnNewsLetter.selected completed:^(id responseObject, NSError *error)
        {
            if([[APIRequest request] isUserNotVerifiedWithResponse:responseObject] || [[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self buttonTitle:CLocalize(@"Ok") handler:^(UIAlertAction *action)
                 {
                     [self popToLoginViewController:self.isFromHome];
                     
                }];
                
            }
            
        }];
        
        
    }

}

-(IBAction)btnNewsLetterClicked:(UIButton*)sender
{
    sender.selected = !sender.selected;
}
-(IBAction)btnPrivacyPolicyClicked:(UIButton*)sender
{
//    MIWebViewViewController *webView = [[MIWebViewViewController alloc] initWithNibName:@"MIWebViewViewController" bundle:nil];
//    webView.title = @"Privacy Policy";
//    [webView setGeneralDataType:GeneralDataTypePrivacyPolicy];
//    webView.view.tag = 0;
//    [self.navigationController pushViewController:webView animated:YES];
  
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ZURU_Privacy_Policy" ofType:@"pdf"];
    MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
    inAppWebVC.iObject = [NSURL fileURLWithPath:path];
    [self presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
}
-(IBAction)btnChildrenPrivacyPolicyClicked:(UIButton*)sender
{
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Childrens_Privacy_Policy" ofType:@"pdf"];
    MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
    inAppWebVC.iObject = [NSURL fileURLWithPath:path];
    [self presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
}
- (IBAction)btnAgePermissionClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
}
- (IBAction)btnPpAndTuClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
}

- (void)setupAttributedText {
    lblPpAndTc.numberOfLines = 0;
    //Select this box to confirm that you consent to our Privacy Policy and Terms of Use.
    NSString *strTC = CLocalize(@"Terms of Use");
    NSString *strPP = CLocalize(@"Privacy Policy");
    
    NSString *string = [NSString stringWithFormat:CLocalize(lblPpAndTc.text),strTC,strPP];
    
    NSRange rangeTC = [string rangeOfString:strTC];
    NSRange rangePP = [string rangeOfString:strPP];
    
    NSDictionary *ppActiveLinkAttributes = @{NSForegroundColorAttributeName : ColorBlue_3576E9, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    NSDictionary *ppLinkAttributes = @{NSForegroundColorAttributeName : ColorBlue_3576E9, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?9:(Is_iPhone_6_PLUS)?13:(IS_IPAD ? 15 : 11);
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttributes:@{NSFontAttributeName : CFontGroBold(fontSize),
                               NSForegroundColorAttributeName : lblPpAndTc.textColor
                               } range:NSMakeRange(0, string.length)];
    
    lblPpAndTc.attributedText = attString;
    
    lblPpAndTc.activeLinkAttributes = ppActiveLinkAttributes;
    lblPpAndTc.linkAttributes = ppLinkAttributes;
    
    NSURL *urlTC = [NSURL URLWithString:@"action://TC"];
    NSURL *urlPP = [NSURL URLWithString:@"action://PP"];
    
    [lblPpAndTc addLinkToURL:urlTC withRange:rangeTC];
    [lblPpAndTc addLinkToURL:urlPP withRange:rangePP];
    
    lblPpAndTc.textColor = [UIColor blackColor];
    lblPpAndTc.delegate = self;
}

//Delegate Method
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([url.absoluteString isEqualToString:@"action://TC"]) {
        
        MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
        inAppWebVC.iObject = [NSURL URLWithString:@"https://www.zuru.com/wp-content/uploads/2017/04/T_C.pdf"];
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
    }
    else if ([url.absoluteString isEqualToString:@"action://PP"]){
        
        MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
        inAppWebVC.iObject = [NSURL URLWithString:@"https://www.zuru.com/wp-content/uploads/2017/03/ZURU-Privacy-Policy.pdf"];
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
    }
}
@end

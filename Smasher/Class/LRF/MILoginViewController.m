//
//  MILoginViewController.m
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MILoginViewController.h"
#import "MIRegisterViewController.h"
#import "MIInAppWebViewController.h"
#import "MIForgotPasswordViewController.h"
#import "TTTAttributedLabel.h"

@interface MILoginViewController ()<TTTAttributedLabelDelegate>

@end

@implementation MILoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
     [UIApplication sharedApplication].statusBarHidden = NO;
    if (IS_IPAD)
    {
        [txtEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(70, 28)];
        [txtPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(70, 35)];
    }
    else
    {
        [txtEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(50, 20)];
        [txtPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(50, 25)];
    }
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_back"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBackClicked:)];
    //
    NSString *string = [CLocalize(@"LET'S GET SMASHING") uppercaseString];
    //....Attributed String
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?17:(Is_iPhone_6_PLUS)?21:(IS_IPAD ? 23 : 19);
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttribute:NSFontAttributeName value:CFontGroBold(fontSize) range:NSMakeRange(0, string.length)];
    [attString addAttribute:NSFontAttributeName value:CFontHalveticaNeueLT(fontSize) range:[string rangeOfString:@"'"]];
    [lblTitle setAttributedText:attString];
    
    [self setupAttributedText];
}

#pragma mark -
#pragma mark - Action Event

-(IBAction)btnSignInClicked:(UIButton*)sender
{
    [self resignKeyboard];
    if(IS_IPHONE_SIMULATOR)
    {
        if(![txtEmail.text isBlankValidationPassed])
            txtEmail.text = @"michalgreg2013@gmail.com";
        
        if(![txtPassword.text isBlankValidationPassed])
            txtPassword.text = @"123456";
        
        
    }
    if(![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageBlankEmail) onView:self];
    
    else if(![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageValidEmail) onView:self];
    
    else if(![txtPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageBlankPassword) onView:self];
   
    else if(!btnAgePermission.isSelected)
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageAgePermission) onView:self];
    
    else if(!btnPpAndTc.isSelected)
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageTermsOfUse) onView:self];
    
    else
    {
        [[APIRequest request] signInWithEmail:txtEmail.text andPassword:txtPassword.text completed:^(id responseObject, NSError *error)
        {
           if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [appDelegate signinUser];
            }
            
        }];        
    }
}
- (IBAction)btnAgePermissionClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
}
- (IBAction)btnPpAndTuClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
}

-(IBAction)btnBackClicked:(UIButton*)sender
{
    [appDelegate popToHomeViewController];
}

-(IBAction)btnCreateAccountClicked:(UIButton*)sender
{
    [appDelegate showSelectDOBPopUp:self];
}

-(IBAction)btnForgotPasswordClicked:(UIButton*)sender
{
    MIForgotPasswordViewController *forgotVC = [[MIForgotPasswordViewController alloc] initWithNibName:@"MIForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:forgotVC animated:YES];

}
- (void)setupAttributedText {
    lblPpAndTc.numberOfLines = 0;
    
    NSString *strTC = CLocalize(@"Terms of Use");
    NSString *strPP = CLocalize(@"Privacy Policy");
    
    NSString *string = [NSString stringWithFormat:CLocalize(lblPpAndTc.text),strTC,strPP];
    
    NSRange rangeTC = [string rangeOfString:strTC];
    NSRange rangePP = [string rangeOfString:strPP];
    
    NSDictionary *ppActiveLinkAttributes = @{NSForegroundColorAttributeName : ColorBlue_3576E9, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    NSDictionary *ppLinkAttributes = @{NSForegroundColorAttributeName : ColorBlue_3576E9, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
   
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?9:(Is_iPhone_6_PLUS)?13:(IS_IPAD ? 15 : 11);
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttributes:@{NSFontAttributeName : CFontGroBold(fontSize),
                               NSForegroundColorAttributeName : lblPpAndTc.textColor
                               } range:NSMakeRange(0, string.length)];
    
    lblPpAndTc.attributedText = attString;
    
    lblPpAndTc.activeLinkAttributes = ppActiveLinkAttributes;
    lblPpAndTc.linkAttributes = ppLinkAttributes;
    
    NSURL *urlTC = [NSURL URLWithString:@"action://TC"];
    NSURL *urlPP = [NSURL URLWithString:@"action://PP"];
    
    [lblPpAndTc addLinkToURL:urlTC withRange:rangeTC];
    [lblPpAndTc addLinkToURL:urlPP withRange:rangePP];
    
    lblPpAndTc.textColor = [UIColor blackColor];
    lblPpAndTc.delegate = self;
}

//Delegate Method
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([url.absoluteString isEqualToString:@"action://TC"]) {
       
        MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
        inAppWebVC.iObject = [NSURL URLWithString:@"https://www.zuru.com/wp-content/uploads/2017/04/T_C.pdf"];
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
    }
    else if ([url.absoluteString isEqualToString:@"action://PP"]){
        
        MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
        inAppWebVC.iObject = [NSURL URLWithString:@"https://www.zuru.com/wp-content/uploads/2017/03/ZURU-Privacy-Policy.pdf"];
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
    }
}
@end

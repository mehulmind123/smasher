//
//  MIEditProfileViewController.m
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIEditProfileViewController.h"
#import "MIFooterView.h"

@interface MIEditProfileViewController ()
{
     MIFooterView *footer;
     NSNumber *countryId;
     UIPickerView *myPickerView;
     NSArray *arrCountryList;
}
@end

@implementation MIEditProfileViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Edit Profile");

    arrCountryList = [CUserDefaults valueForKey:UserDefaultCountryList];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %d", appDelegate.loginUser.country_id];
    NSDictionary *dict  = [[arrCountryList filteredArrayUsingPredicate:predicate] firstObject];

    if(appDelegate.loginUser)
    {
        [txtEmail setText:appDelegate.loginUser.email];
        [txtName setText:appDelegate.loginUser.user_name];
        [txtCountry setText:[dict stringValueForJSON:@"name"]];
        countryId = [NSNumber numberWithInteger:appDelegate.loginUser.country_id] ;
        
        
        //...
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [dateFormatter setDateFormat:@"yyyy/MM/dd"];
        NSDate *birthDate = [dateFormatter dateFromString:appDelegate.loginUser.dob];
        [dateFormatter setDateFormat:@"dd MMMM, yyyy"];
        
        NSLog(@"%@",[dateFormatter stringFromDate:birthDate]);
        [txtDOB setText:[dateFormatter stringFromDate:birthDate]];
        
    }
    

    if (IS_IPAD)
    {
        [txtName setLeftImage:[UIImage imageNamed:@"name"] withSize:CGSizeMake(70, 35)];
        [txtEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(70, 28)];
        
        [txtDOB setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(70, 35)];
        [txtCountry setLeftImage:[UIImage imageNamed:@"country"] withSize:CGSizeMake(70, 35)];
        [txtCountry setRightImage:[UIImage imageNamed:@"dropdown_blue"] withSize:CGSizeMake(50, 15)];
    }
    else
    {
        [txtName setLeftImage:[UIImage imageNamed:@"name"] withSize:CGSizeMake(50, 25)];
        [txtEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(50, 20)];
        
        [txtDOB setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(50, 25)];
        [txtCountry setLeftImage:[UIImage imageNamed:@"country"] withSize:CGSizeMake(50, 25)];
        [txtCountry setRightImage:[UIImage imageNamed:@"dropdown_blue"] withSize:CGSizeMake(28, 8)];
    }

    
//    [txtCountry setPickerData:[arrCountryList valueForKeyPath:@"name"] update:^(NSString *text, NSInteger row, NSInteger component)
//     {
//
//         countryId = arrCountryList[row][@"id"];
//     }];

    [[APIRequest request] loadCountryList_completed:^(id responseObject, NSError *error)
     {
         arrCountryList = [CUserDefaults valueForKey:UserDefaultCountryList];
         
     }];
    txtCountry.delegate = self;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        footer = [MIFooterView footer];
        [vWFooter addSubview:footer];
    });

}

-(void)addPickerView
{
    
    myPickerView = [[UIPickerView alloc]init];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    txtCountry.inputView = myPickerView;
    UIButton *btnDone = [[UIButton alloc] initWithFrame:CGRectMake(CScreenWidth - (IS_IPAD ? 80:60), 0, (IS_IPAD ? 80:50), 50)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone.titleLabel setFont:[UIFont boldSystemFontOfSize:(IS_IPAD ? 21 : 17)]];
    [btnDone setTitleColor:CRGB(0, 0, 0) forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(btnDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lblPlaceholer = [[UILabel alloc] initWithFrame:CGRectMake(60, 0,CScreenWidth - 120 , 50 )];
    lblPlaceholer.text = txtCountry.placeholder;
    lblPlaceholer.textColor = CRGB(171, 171, 171);
    lblPlaceholer.font = [UIFont systemFontOfSize:(IS_IPAD ? 18 : 14)];
    lblPlaceholer.textAlignment = NSTextAlignmentCenter;
    UIView *vWAccessor = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-
                                                                  myPickerView.frame.size.height - 50, CScreenWidth, 50)];
    [vWAccessor addSubview:btnDone];
    [vWAccessor addSubview:lblPlaceholer];
    vWAccessor.backgroundColor = CRGB(247, 247, 247);
    txtCountry.inputAccessoryView = vWAccessor;
    
    if ([txtCountry.text isBlankValidationPassed])
    {
        [myPickerView reloadAllComponents];
        [myPickerView selectRow:[[arrCountryList valueForKeyPath:@"name"] indexOfObject:txtCountry.text] inComponent:0 animated:false];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self addPickerView];
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return arrCountryList.count;
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    
    [txtCountry setText:[[arrCountryList objectAtIndex:row] stringValueForJSON:@"name"] ];
    countryId = arrCountryList[row][@"id"];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
    return [[arrCountryList objectAtIndex:row] stringValueForJSON:@"name"];
}


#pragma mark -
#pragma mark - Action Event
-(IBAction)btnDoneClicked:(UIButton*)sender
{
    [myPickerView removeFromSuperview];
    myPickerView = nil;
    [txtCountry resignFirstResponder ];
}

-(IBAction)btnUpdateClicked:(UIButton*)sender
{
    if(![txtName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageBlankUserName) onView:self];
    else if(![txtCountry.text  isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageSelectCountry) onView:self];
    else
    {
        if(![appDelegate.loginUser.user_name isEqualToString:txtName.text] || ![[NSNumber numberWithInteger:appDelegate.loginUser.country_id] isEqual:countryId])
        {
            [[APIRequest request] editProfileWithUserName:txtName.text andCountry:countryId andSignUpToNewsLetter:btnNewsLetter.selected completed:^(id responseObject, NSError *error)
             {
                 if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self buttonTitle:CLocalize(@"Ok") handler:^(UIAlertAction *action)
                      {
                          [appDelegate popToHomeViewController];

                          
                      }];
                 }
             }];
        }
        else
            [appDelegate popToHomeViewController];
    }
    
}
-(IBAction)btnNewsLetterClicked:(UIButton*)sender
{
    
    sender.selected = !sender.selected;
    
}
-(IBAction)btnCancelClicked:(UIButton*)sender
{
    [appDelegate popToHomeViewController];
}

@end

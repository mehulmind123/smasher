//
//  MIForgotPasswordViewController.m
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIForgotPasswordViewController.h"

@interface MIForgotPasswordViewController ()

@end

@implementation MIForgotPasswordViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Forgot Password");
    lblDetail.text = CLocalize(@"Enter your registered email address below to reset your password. You will receive a confirmation email shortly.");
    [txtEmail setLeftImage:[UIImage imageNamed:@"email"] withSize:CGSizeMake(50, 20)];
}

#pragma mark -
#pragma mark - Action Event

-(IBAction)btnSubmitClicked:(UIButton*)sender
{
    [self resignKeyboard];
    if(![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageBlankEmail) onView:self];
    
    else if(![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageValidEmail) onView:self];
    
    else
    {
        [[APIRequest request] forgotPasswordWithEmail:txtEmail.text completed:^(id responseObject, NSError *error)
        {
            if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self buttonTitle:CLocalize(@"Ok") handler:^(UIAlertAction *action)
                 {
                     [self.navigationController popViewControllerAnimated:YES];
                     
                 }];
            }
            
        }];
        
    }
    
}

#pragma mark -
#pragma mark - General Method
@end

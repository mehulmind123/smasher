//
//  MIChangePasswordViewController.m
//  Smasher
//
//  Created by Mac-00014 on 29/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIChangePasswordViewController.h"
#import "MIFooterView.h"
@interface MIChangePasswordViewController ()
{
    MIFooterView *footer;
}
@end

@implementation MIChangePasswordViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.title = CLocalize(@"Change Password");
    if (IS_IPAD)
    {
        [txtOldPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(70, 35)];
        [txtNewPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(70, 35)];
        [txtConfirmNewPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(70, 35)];
    }
    else
    {
        [txtOldPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(50, 25)];
        [txtNewPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(50, 25)];
        [txtConfirmNewPassword setLeftImage:[UIImage imageNamed:@"password"] withSize:CGSizeMake(50, 25)];
    }
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        footer = [MIFooterView footer];
        [vWFooter addSubview:footer];
    });
    
}

#pragma mark -
#pragma mark - Action Event

-(IBAction)btnSubmitClicked:(UIButton*)sender
{
    [self resignKeyboard];
    if(![txtOldPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageEnterOldPassword) onView:self];

    if(![txtNewPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageEnterNewPassword) onView:self];

    else if(![txtNewPassword.text isAlphaNumericValidationPassed] || txtNewPassword.text.length < 6)
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageAlphaNumbricPassword) onView:self];

    else if(![txtConfirmNewPassword.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessageEnterConfirmPassword) onView:self];
    
    else if(![txtConfirmNewPassword.text  isEqualToString:txtNewPassword.text])
        [CustomAlertView iOSAlert:@"" withMessage:CLocalize(CMessagePasswordMisMatch) onView:self];

    else
    {
        [[APIRequest request] changePasswordWithNewPassword:txtNewPassword.text andOldPassword:txtOldPassword.text completed:^(id responseObject, NSError *error)
        {
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
            {
                [CustomAlertView iOSAlert:@"" withMessage:[[responseObject valueForKey:CJsonMeta] stringValueForJSON:CJsonMessage] onView:self buttonTitle:CLocalize(@"Ok") handler:^(UIAlertAction *action)
                {
                    [appDelegate popToHomeViewController];
                }];
            }
            
        }];
    }
}

@end

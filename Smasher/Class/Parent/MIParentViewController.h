//
//  MIParentViewController.h
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSUInteger, StopAnimationType)
{
    StopAnimationTypeDataNotFound,
    StopAnimationTypeErrorTapToRetry,
    StopAnimationTypeRemove
};

@interface MIParentViewController : UIViewController

@property (nonatomic, assign) BOOL isPresentedVC;

@property (nonatomic, strong) id iObject;

@property (nonatomic, copy) void(^touchUpInsideViewClicked)();

- (void)setStatusBarHidden:(BOOL)hidden;

- (void)setStatusBarStyle:(UIStatusBarStyle)style;

- (void)resignKeyboard;

- (void)startLoadingAnimationInView:(UIView *)view;

- (void)stopLoadingAnimationInView:(UIView *)view type:(StopAnimationType)stopAnimationType message:(NSString*)message touchUpInsideClickedEvent:(void(^)())completion;

- (void)showNoDataFoundInView:(UIView*)view message:(NSString*)message;
- (void)removeNodataFoundInView:(UIView*)view;

@end

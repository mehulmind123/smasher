//
//  MIBubbleViewLayout.m
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIBubbleViewLayout.h"

#define kDefaultInterItemSpacing    5.0f
#define kDefaultLineSpacing         10.0f

@interface MIBubbleViewLayout ()

@property (nonatomic, strong) NSDictionary *itemLayoutDictionary;
@property (nonatomic) CGSize contentSize;
@property (nonatomic, weak) id <MIBubbleViewLayoutDelegate> delegate;

@end


@implementation MIBubbleViewLayout

- (id)initWithDelegate:(id <MIBubbleViewLayoutDelegate>)delegate
{
    self = [super init];
    if (self)
    {
        self.delegate = delegate;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.minimumLineSpacing = kDefaultLineSpacing;
        self.minimumInteritemSpacing = kDefaultInterItemSpacing;
    }
    return self;
}

- (void)prepareLayout
{
    [super prepareLayout];
    
    if (self.collectionView.numberOfSections == 0 ||
        [self.collectionView numberOfItemsInSection:0] == 0)
        return;
    
    if (self.itemLayoutDictionary)
        self.itemLayoutDictionary = nil;
    
    NSMutableDictionary *itemLayoutInformation = [NSMutableDictionary dictionary];
    
    CGFloat x = 0.0f;
    CGFloat y = 0.0f;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    NSInteger numberOfItems = [self.collectionView numberOfItemsInSection:0];
    CGSize iSize;
    
    for (int itemIndex = 0; itemIndex < numberOfItems; itemIndex ++)
    {
        indexPath = [NSIndexPath indexPathForItem:itemIndex inSection:0];
        iSize = [self.delegate collectionView:self.collectionView itemSizeAtIndexPath:indexPath];
        
        
        CGRect itemRect = CGRectMake(x, y, iSize.width, iSize.height);
        if (x > 0 && x + iSize.width + self.minimumInteritemSpacing > self.collectionView.frame.size.width)
        {
            itemRect.origin.x = 0.0f;
            itemRect.origin.y = y + iSize.height + self.minimumLineSpacing;
        }
        
        
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attributes.frame = itemRect;
        [itemLayoutInformation setObject:attributes forKey:indexPath];
        
        x = itemRect.origin.x + iSize.width + self.minimumInteritemSpacing;
        y = itemRect.origin.y;
    }
    y += iSize.height;
    x = 0.0f;
    
    
    self.contentSize = CGSizeMake(self.collectionView.frame.size.width, y + self.minimumLineSpacing);
    self.itemLayoutDictionary = [NSDictionary dictionaryWithDictionary:itemLayoutInformation];
}

- (CGSize)collectionViewContentSize
{
    return self.contentSize;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *attributes = [NSMutableArray array];
    
    NSInteger numberOfItems = [self.collectionView numberOfItemsInSection:0];
    for (int itemIndex = 0; itemIndex < numberOfItems; itemIndex ++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:itemIndex inSection:0];
        UICollectionViewLayoutAttributes *cellAttribs = [self layoutAttributesForItemAtIndexPath:indexPath];
        [attributes addObject:cellAttribs];
    }
    
    return attributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [self.itemLayoutDictionary objectForKey:indexPath];
    return attributes;
}

@end

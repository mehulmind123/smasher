//
//  MIGenericCollectionView.m
//  VoxPop
//
//  Created by mac-0007 on 23/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericCollectionView.h"

@implementation MIGenericCollectionView

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    if (!CGSizeEqualToSize(self.bounds.size, [self intrinsicContentSize]))
        [self invalidateIntrinsicContentSize];
}

- (CGSize)intrinsicContentSize
{
    CGSize intrinsicContentSize = self.contentSize;
    return intrinsicContentSize;
}

@end

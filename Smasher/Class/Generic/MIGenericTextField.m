//
//  MIGenericTextField.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericTextField.h"

@implementation MIGenericTextField

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (instancetype)init
{
    self = [super init];
    if (self) [self initialize];
    return self;
}

- (void)initialize
{
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    if (Is_iPhone_4 || Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 2)];
    else if (Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 2)];
    else if (IS_IPAD)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 4)];
    
    [self setPlaceholder:CLocalize(self.placeholder)];
    
    if (self.tag == 500)
    {
        self.layer.cornerRadius = 5;
        self.layer.shadowColor = ColorBlue_64E1FC.CGColor;
        self.layer.shadowOffset = CGSizeMake(2,2);
        self.layer.shadowRadius = 0.0f;
        self.layer.shadowOpacity = 1.0f;
        
        [self setPlaceHolderColor:ColorBlue_1493B3];
    }

}


@end

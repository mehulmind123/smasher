//
//  MIGenericButton.m
//  VoxPop
//
//  Created by mac-0007 on 21/08/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIGenericButton.h"

@interface MIGenericButton ()
{
    CAGradientLayer *gradient;
}
@end

@implementation MIGenericButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (instancetype)init
{
    self = [super init];
    if (self) [self initialize];
    return self;
}

- (void)initialize
{
    NSString *fontName = self.titleLabel.font.fontName;
    CGFloat fontSize = self.titleLabel.font.pointSize;
    
    if (Is_iPhone_4 || Is_iPhone_5)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 2)];
    else if (Is_iPhone_6_PLUS)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 2)];
    else if (IS_IPAD)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 4)];
    
    if (IS_IPAD && self.tag == 101)
        self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize + 8)];
    
    if (self.tag == 699 && appDelegate.isSpanish) {
        //..... 699  For Uppercase text
        
        if (Is_iPhone_4 || Is_iPhone_5)
            self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 4)];
        else
            self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 2)];
       
        [self setUpperCaseString];
    }
    else if (self.tag == 101 && appDelegate.isSpanish)
    {
        if (!IS_IPAD)
         self.titleLabel.font = [UIFont fontWithName:fontName size:(fontSize - 2)];
        
        [self setUpperCaseString];
    }
    else
        [self setTitle:CLocalize(self.currentTitle) forState:UIControlStateNormal];
    
    
    if (self.tag == 500)
    {
        [self setUpperCaseString];
        self.layer.cornerRadius = 5;
        self.layer.shadowColor = ColorRed_D12015.CGColor;
        self.layer.shadowOffset = CGSizeMake(2,2);
        self.layer.shadowRadius = 0.0f;
        self.layer.shadowOpacity = 1.0f;
    }
    if (self.tag == 501)
    {
        [self setUpperCaseString];
        self.layer.cornerRadius = 5;
        self.layer.shadowColor = ColorBlue_205391.CGColor;
        self.layer.shadowOffset = CGSizeMake(2,2);
        self.layer.shadowRadius = 0.0f;
        self.layer.shadowOpacity = 1.0f;
    }
//    else
//    {
//        self.layer.shadowOffset = CGSizeZero;
//        self.layer.shadowRadius = 6;
//        self.layer.shadowOpacity = 0.1;
//    }

}

- (void)setUpperCaseString
{
    [self setTitle:[CLocalize(self.currentTitle) uppercaseString] forState:UIControlStateNormal];
}

@end

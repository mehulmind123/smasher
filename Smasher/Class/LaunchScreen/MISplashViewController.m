//
//  MISplashViewController.m
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MISplashViewController.h"

@interface MISplashViewController ()

@end

@implementation MISplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setStatusBarHidden:YES];
    
    if (IS_IPAD)
        [_imgVSplash setImage:[UIImage imageNamed:@"splash_ipad"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setStatusBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end

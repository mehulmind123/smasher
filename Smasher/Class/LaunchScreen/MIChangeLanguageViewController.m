//
//  MIChangeLanguageViewController.m
//  Smasher
//
//  Created by Mac-00014 on 22/01/18.
//  Copyright © 2018 Ishwar-00014. All rights reserved.
//

#import "MIChangeLanguageViewController.h"

@interface MIChangeLanguageViewController ()

@end

@implementation MIChangeLanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    if([CUserDefaults valueForKey:UserDefaultCurrentLanguage])
    {
        [UIApplication sharedApplication].statusBarHidden = NO;
    }
    [txtEnglish setLeftImage:nil withSize:CGSizeMake(20, 20)];
    [txtSpanish setLeftImage:nil withSize:CGSizeMake(20, 20)];
    
    //
    NSString *string = CLocalize(@"SELECT LANGUAGE");
    //....Attributed String
    CGFloat fontSize = (Is_iPhone_4 || Is_iPhone_5)?17:(Is_iPhone_6_PLUS)?21:(IS_IPAD ? 23 : 19);
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttribute:NSFontAttributeName value:CFontGroBold(fontSize) range:NSMakeRange(0, string.length)];
    [attString addAttribute:NSFontAttributeName value:CFontHalveticaNeueLT(fontSize) range:[string rangeOfString:@"'"]];
    [lblTitle setAttributedText:attString];
    
    if([[CUserDefaults valueForKey:UserDefaultCurrentLanguage] isEqualToString:ClanguageSpanish])
    {
        btnSpanish.selected = true;
        btnEnglish.selected = false;
    }
    else
    {
        btnSpanish.selected = false;
        btnEnglish.selected = true;
    }
        
}

#pragma mark -
#pragma mark - Action event

- (IBAction)btnSelectLanguageClicked:(UIButton*)sender
{
    btnEnglish.selected = btnSpanish.selected = false;
    sender.selected = true;
    
}

- (IBAction)btnSubmitClicked:(UIButton*)sender
{
    if(![CUserDefaults valueForKey:UserDefaultCurrentLanguage])
    {
        if (btnEnglish.isSelected)
        {
            appDelegate.isSpanish = NO;
            [CUserDefaults setValue:ClanguageEnglish forKey:UserDefaultCurrentLanguage];
            [CUserDefaults synchronize];
            [[LocalizationSystem sharedLocalSystem] setLanguage:ClanguageEnglish];
        }
        else
        {
            appDelegate.isSpanish = YES;
            [CUserDefaults setValue:ClanguageSpanish forKey:UserDefaultCurrentLanguage];
            [CUserDefaults synchronize];
            [[LocalizationSystem sharedLocalSystem] setLanguage:ClanguageSpanish];
        }
        
        [appDelegate initiateLoadingScreen];
        
    }
    else
    {
        if (![[CUserDefaults valueForKey:UserDefaultCurrentLanguage] isEqualToString:ClanguageEnglish] && btnEnglish.isSelected)
        {
            //... language change from Spanish to English
            appDelegate.isSpanish = NO;
            [CUserDefaults setValue:ClanguageEnglish forKey:UserDefaultCurrentLanguage];
            [CUserDefaults synchronize];
            [[LocalizationSystem sharedLocalSystem] setLanguage:ClanguageEnglish];
            [[APIRequest request] loadGeneralData_completed:nil];
            [[APIRequest request] loadCountryList_completed:nil];
            
        }
       else if (![[CUserDefaults valueForKey:UserDefaultCurrentLanguage] isEqualToString:ClanguageSpanish]  && btnSpanish.isSelected)
        {
            //... language change from English to Spanish
            appDelegate.isSpanish = YES;
            [CUserDefaults setValue:ClanguageSpanish forKey:UserDefaultCurrentLanguage];
            [CUserDefaults synchronize];
            [[LocalizationSystem sharedLocalSystem] setLanguage:ClanguageSpanish];
            
            [[APIRequest request] loadGeneralData_completed:nil];
            [[APIRequest request] loadCountryList_completed:nil];
            
        }
        else
        {
            //... language not changed, same as Previous selction
            
        }
        
         [appDelegate popToHomeViewController];
    }
}
@end

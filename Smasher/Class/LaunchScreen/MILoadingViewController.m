//
//  MILoadingViewController.m
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MILoadingViewController.h"

@interface MILoadingViewController ()

@end

@implementation MILoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setStatusBarHidden:YES];
//    [self addLayerAnimation];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self addLayerAnimation];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self setStatusBarHidden:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    [self setStatusBarHidden:YES];

//    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
//    animation.fromValue = [NSNumber numberWithFloat:0.0f];
//    animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
//    animation.duration = 1.0f;
//    animation.repeatCount = INFINITY;
//
//    [imgVLoader.layer addAnimation:animation forKey:@"SpinAnimation"];
    
   
}

- (void)addLayerAnimation
{
    CAShapeLayer *circle = [CAShapeLayer layer];
    circle.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CViewWidth(vWBackgroundImg)/2, CViewWidth(vWBackgroundImg)/2) radius:CViewWidth(vWBackgroundImg)/2 startAngle:-M_PI_2 endAngle:2 * M_PI - M_PI_2 clockwise:YES].CGPath;
    circle.fillColor = [UIColor clearColor].CGColor;
    circle.strokeColor = ColorRed_E93429.CGColor;
    circle.lineWidth = 8;
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    animation.duration = 3;
    animation.removedOnCompletion = NO;
    animation.fromValue = @(0);
    animation.toValue = @(1);
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [circle addAnimation:animation forKey:@"drawCircleAnimation"];
    
    vWBackgroundImg.layer.cornerRadius = CViewWidth(vWBackgroundImg)/2;
    vWBackgroundImg.layer.masksToBounds = YES;
    [vWBackgroundImg.layer addSublayer:circle];
    
}
@end

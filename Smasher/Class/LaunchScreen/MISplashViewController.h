//
//  MISplashViewController.h
//  Smasher
//
//  Created by Mac-00014 on 25/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"

@interface MISplashViewController : MIParentViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgVSplash;

@end

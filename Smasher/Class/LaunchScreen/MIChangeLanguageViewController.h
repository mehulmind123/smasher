//
//  MIChangeLanguageViewController.h
//  Smasher
//
//  Created by Mac-00014 on 22/01/18.
//  Copyright © 2018 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIChangeLanguageViewController : MIParentViewController
{
    IBOutlet UITextField *txtEnglish;
    IBOutlet UITextField *txtSpanish;
    IBOutlet UIButton *btnEnglish;
    IBOutlet UIButton *btnSpanish;
    IBOutlet UILabel *lblTitle;
}
@end

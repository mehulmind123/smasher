//
//  MIHomeHeaderView.h
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HomeEventBlock)(NSString *, NSInteger);

@interface MIHomeHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIImageView *imgVHomeHeader;

@property (nonatomic, strong) IBOutlet UIButton *btnSearch;
@property (nonatomic, strong) IBOutlet UIButton *btnIOwn;
@property (nonatomic, strong) IBOutlet UIButton *btnIWant;
@property (nonatomic, strong) IBOutlet UIButton *btnFilter;

@property (nonatomic, strong) IBOutlet UITextField *txtSearch;

@property (nonatomic, strong) IBOutlet UILabel *lblUserName;
@property (nonatomic, strong) IBOutlet UILabel *lblIOwn;
@property (nonatomic, strong) IBOutlet UILabel *lblIWant;

@property (nonatomic, strong) IBOutlet UILabel *lblYourSmashPoint;
@property (nonatomic, strong) IBOutlet UILabel *lblTitleIOwn;
@property (nonatomic, strong) IBOutlet UILabel *lblTitleIWant;

@property (nonatomic, strong) IBOutlet UIView  *viewSearch;

@property (nonatomic, copy) HomeEventBlock HomeEventBlock;

- (id)initWithInfo:(NSDictionary*)dict handler:(HomeEventBlock)handler;
- (void)configure;
@end

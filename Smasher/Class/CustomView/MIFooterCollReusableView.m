//
//  MIFooterCollReusableView.m
//  Smasher
//
//  Created by Mac-00014 on 06/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIFooterCollReusableView.h"
#import "MIInAppWebViewController.h"

@implementation MIFooterCollReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configuration];
}


- (void)configuration
{
    [self.btnZuru touchUpInsideClicked:^{
        [self presentInAppWebView:@"" url:CFooterVisitZuruUrl];
        
    }];
    
    [self.btnVisit touchUpInsideClicked:^{
        [self presentInAppWebView:@"" url:CFooterVisitZuruUrl];
        
    }];
    
    [self.btnBuy touchUpInsideClicked:^{
        
        [self presentInAppWebView:@"" url:CFooterWhereToBuyUrl];
    }];
    
    [self.btnSafety touchUpInsideClicked:^{
        
        [self presentInAppWebView:@"" url:CFooterKindsSafetyUrl];
    }];
    
    [self.btnContactUs touchUpInsideClicked:^{
        
        [self presentInAppWebView:@"" url:CFooterContactUsUrl];
    }];
}

- (void)presentInAppWebView:(NSString*)title url:(NSString*)url
{
    MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
    inAppWebVC.title = title;
    inAppWebVC.iObject = [NSURL URLWithString:url];
    [self.viewController presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
    
}
@end

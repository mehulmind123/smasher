//
//  MIFooterView.m
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIFooterView.h"
#import "MIInAppWebViewController.h"

@implementation MIFooterView

+ (id)footer
{
    MIFooterView *footer = [[[NSBundle mainBundle] loadNibNamed:@"MIFooterView" owner:nil options:nil] lastObject];
    
    CViewSetWidth(footer, CScreenWidth);
    CViewSetHeight(footer,(CScreenWidth*64)/ 375);
    
    [footer configuration];
    return footer;
    
}

- (void)configuration
{
    [self.btnZuru touchUpInsideClicked:^{
        [self presentInAppWebView:@"" url:CFooterVisitZuruUrl];
        
    }];
    
    [self.btnVisit touchUpInsideClicked:^{
        [self presentInAppWebView:@"" url:CFooterVisitZuruUrl];
        
    }];
    
    [self.btnBuy touchUpInsideClicked:^{
        
        [self presentInAppWebView:@"" url:CFooterWhereToBuyUrl];
    }];
    
    [self.btnSafety touchUpInsideClicked:^{

        [self presentInAppWebView:@"" url:CFooterKindsSafetyUrl];
    }];
    
    [self.btnContactUs touchUpInsideClicked:^{

        [self presentInAppWebView:@"" url:CFooterContactUsUrl];
    }];
}

- (void)presentInAppWebView:(NSString*)title url:(NSString*)url
{
    MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
    inAppWebVC.title = title;
    inAppWebVC.iObject = [NSURL URLWithString:url];
    [self.viewController presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];

}
@end

//
//  MIFooterCollReusableView.h
//  Smasher
//
//  Created by Mac-00014 on 06/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIFooterCollReusableView : UICollectionReusableView

@property (nonatomic, strong) IBOutlet UIButton *btnVisit;
@property (nonatomic, strong) IBOutlet UIButton *btnBuy;
@property (nonatomic, strong) IBOutlet UIButton *btnSafety;
@property (nonatomic, strong) IBOutlet UIButton *btnContactUs;
@property (nonatomic, strong) IBOutlet UIButton *btnZuru;
@property (nonatomic, strong) IBOutlet UIView   *vWContent;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint   *vWContentTop;

- (void)configuration;
@end

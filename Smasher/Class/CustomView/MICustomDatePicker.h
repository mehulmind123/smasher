//
//  MICustomDatePicker.h
//  Smasher
//
//  Created by Mac-00014 on 02/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DatePickerEventBlock)(NSDate*);

@interface MICustomDatePicker : UIView
@property (nonatomic, copy) DatePickerEventBlock DatePickerEventBlock;

- (id)initWithDate:(NSDate*)selectedDate handler:(DatePickerEventBlock)handler;
@end

//
//  MILoginPopView.m
//  Smasher
//
//  Created by Mac-00014 on 02/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MILoginPopView.h"

@interface MILoginPopView ()

@property (nonatomic, strong) IBOutlet UIButton *btnLogin;
@property (nonatomic, strong) IBOutlet UIButton *btnCreateAccount;
@property (nonatomic, strong) IBOutlet UIButton *btnClose;

@property (nonatomic, strong) IBOutlet UILabel *lblTitile;
@property (nonatomic, strong) IBOutlet UILabel *lblDiscription;

@property (nonatomic, strong) IBOutlet UIView  *vWContent;

@end

@implementation MILoginPopView

- (id)initWithInfo:(NSString*)message handler:(LoginEventBlock)handler
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"MILoginPopView" owner:nil options:nil] lastObject];
    
    self.LoginEventBlock  = handler;
    
    CViewSetWidth(self, CScreenWidth*(IS_IPAD ? 0.7: 0.92));
    CGFloat vHeight = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.lblDiscription.text = message.isBlankValidationPassed ? message : CLocalize(CMessageLoginRequire);
    CGFloat vHeightLbl = [self getLabelHeight:_lblDiscription];
    CViewSetHeight(self,vHeight+vHeightLbl+20);
    
    if(IS_IPAD)
        _lblDiscription.font = [UIFont fontWithName:_lblDiscription.font.fontName size:(_lblDiscription.font.pointSize + 2)];
    
    [self initialize];
    return self;
}

#pragma mark -
#pragma mark - Helper method

- (void)initialize
{
    self.btnLogin.layer.cornerRadius =
    self.btnCreateAccount.layer.cornerRadius = 5;
    
    self.btnLogin.layer.borderWidth =
    self.btnCreateAccount.layer.borderWidth = 1;
    
    self.btnLogin.layer.borderColor = ColorBlue_2235A0.CGColor;
    self.btnCreateAccount.layer.borderColor = ColorRed_D12015.CGColor;
    
    self.vWContent.layer.cornerRadius = 10;
    self.btnClose.layer.cornerRadius = CViewWidth(self.btnClose)/2;
    
    [_btnLogin setTitle:[CLocalize(@"LOGIN") uppercaseString] forState:UIControlStateNormal];
    [_btnCreateAccount setTitle:[CLocalize(@"CREATE ACCOUNT") uppercaseString] forState:UIControlStateNormal];
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
#pragma mark -
#pragma mark - Action Event

- (IBAction)btnCloseClicked:(UIButton*)sender
{
    if(self.LoginEventBlock)
        self.LoginEventBlock(0);
}

- (IBAction)btnLoginClicked:(UIButton*)sender
{
    if(self.LoginEventBlock)
        self.LoginEventBlock(1);
}

- (IBAction)btnCreateAccountClicked:(UIButton*)sender
{
    if(self.LoginEventBlock)
        self.LoginEventBlock(2);
}


@end

//
//  MIFooterView.h
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIFooterView : UIView

@property (nonatomic, strong) IBOutlet UIButton *btnVisit;
@property (nonatomic, strong) IBOutlet UIButton *btnBuy;
@property (nonatomic, strong) IBOutlet UIButton *btnSafety;
@property (nonatomic, strong) IBOutlet UIButton *btnContactUs;
@property (nonatomic, strong) IBOutlet UIButton *btnZuru;

+ (id)footer;

@end

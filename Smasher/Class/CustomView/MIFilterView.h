//
//  MIFilterView.h
//  Smasher
//
//  Created by Mac-00014 on 27/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^FilterEventBlock)(NSDictionary*);

@interface MIFilterView : UIView
@property (nonatomic, copy) FilterEventBlock FilterEventBlock;

- (id)initWithSelectedOption:(NSDictionary*)dictSelected handler:(FilterEventBlock)handler;
@end

//
//  MICustomDatePicker.m
//  Smasher
//
//  Created by Mac-00014 on 02/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MICustomDatePicker.h"

@interface MICustomDatePicker()

@property (nonatomic, strong)IBOutlet UIDatePicker *datePicker;

@end

@implementation MICustomDatePicker

- (id)initWithDate:(NSDate*)selectedDate handler:(DatePickerEventBlock)handler
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"MICustomDatePicker" owner:nil options:nil] lastObject];
    self.DatePickerEventBlock  = handler;
   
    CViewSetWidth(self, CScreenWidth*(IS_IPAD ? 0.6:0.8));
    CGFloat vHeight = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CViewSetHeight(self,vHeight);
    
    [self initialize];
    return self;
}

#pragma mark -
#pragma mark - Helper method

- (void)initialize
{
     self.layer.cornerRadius = 10;
    
    //...
    NSCalendar* calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]];
    NSString *dateString = [NSString stringWithFormat:@"01/01/%ld",[components year]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *date = [dateFormat dateFromString: dateString];
    //...
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    [self.datePicker setMaximumDate:[NSDate date]];
    self.datePicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    [self.datePicker setDate:date];
    [self.datePicker setLocale: [NSLocale localeWithLocaleIdentifier:appDelegate.isSpanish?@"es":@"en"]];
    
}
#pragma mark -
#pragma mark - Action Event

-(IBAction)btnDoneClicked:(UIButton*)sender
{
    if(self.DatePickerEventBlock)
        self.DatePickerEventBlock(self.datePicker.date);
}

-(IBAction)btnCancleClicked:(UIButton*)sender
{
    if(self.DatePickerEventBlock)
        self.DatePickerEventBlock(nil);

}


@end

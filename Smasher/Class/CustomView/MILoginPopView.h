//
//  MILoginPopView.h
//  Smasher
//
//  Created by Mac-00014 on 02/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^LoginEventBlock)(NSInteger);
@interface MILoginPopView : UIView

@property (nonatomic, copy) LoginEventBlock LoginEventBlock;

- (id)initWithInfo:(NSString*)message handler:(LoginEventBlock)handler;
@end

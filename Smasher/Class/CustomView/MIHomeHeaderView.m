//
//  MIHomeHeaderView.m
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIHomeHeaderView.h"

@interface MIHomeHeaderView()

@property (nonatomic, strong) NSDictionary *dictInfo;

@end

@implementation MIHomeHeaderView

- (id)initWithInfo:(NSDictionary*)dict handler:(HomeEventBlock)handler
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"MIHomeHeaderView" owner:nil options:nil] lastObject];
    
    self.HomeEventBlock  = handler;
    self.dictInfo = dict;
    CViewSetWidth(self, CScreenWidth);
    CViewSetHeight(self,(CScreenWidth*250)/ 375);
    
    return self;
}


- (void)configure
{
    
    [self.viewSearch.layer setCornerRadius:CViewHeight(self.viewSearch)/2];
    [self.viewSearch.layer setBorderWidth:1.0];
    [self.viewSearch.layer setBorderColor:ColorBlue_6BCBF3.CGColor];
    
    [self.btnFilter.layer setCornerRadius:CViewHeight(self.btnFilter)/2];
    [self.btnFilter.layer setBorderWidth:1.5];
    [self.btnFilter.layer setBorderColor:ColorBlue_6BCBF3.CGColor];
    
    if (IS_IPAD)
    {
        _txtSearch.font = [UIFont fontWithName:_txtSearch.font.fontName size:20];
        [self.btnFilter.titleLabel setFont:[UIFont fontWithName:_txtSearch.font.fontName size:20]];
    }
}



@end

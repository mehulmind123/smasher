//
//  MIFilterSectionHeader.h
//  Smasher
//
//  Created by Mac-00014 on 27/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIFilterSectionHeader : UITableViewHeaderFooterView

@property (nonatomic, weak)IBOutlet UILabel *lblTitle;

@property (nonatomic, weak)IBOutlet UIButton *btnDropDown;

@property (nonatomic, weak)IBOutlet UIView *vWContent;

@end

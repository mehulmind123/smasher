//
//  MIFilterView.m
//  Smasher
//
//  Created by Mac-00014 on 27/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIFilterView.h"
#import "MIFilterSectionHeader.h"
#import "MIFilterTableViewCell.h"

@interface MIFilterView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)IBOutlet UITableView *tblFilter;
@property (nonatomic, strong) NSMutableDictionary *dictSelected;
@property (nonatomic, strong) NSMutableDictionary *dictFilter;
@property (nonatomic, strong) NSMutableArray *arrFilter;
@property (nonatomic, strong) IBOutlet UIButton *btnRefresh;

@end
@implementation MIFilterView

- (id)initWithSelectedOption:(NSDictionary*)dictSelected handler:(FilterEventBlock)handler
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"MIFilterView" owner:nil options:nil] lastObject];
    
    self.FilterEventBlock  = handler;
    
    self.dictSelected = [NSMutableDictionary new];
    [self.dictSelected addEntriesFromDictionary:dictSelected];
    
    CViewSetWidth(self, CScreenWidth);
    CViewSetHeight(self,IS_IPAD ? (CScreenHeight*0.75) : ((CScreenWidth*490)/ 375));
    [self initialize];
    return self;

}

- (void)initialize
{
    UIImage *image = [[UIImage imageNamed:@"reset"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.btnRefresh setImage:image forState:UIControlStateNormal];
    self.btnRefresh.tintColor = CRGB(14, 110, 184);
    
    _arrFilter = [NSMutableArray new];
    
    _dictFilter = [NSMutableDictionary new];
    [_dictFilter addEntriesFromDictionary:[CUserDefaults valueForKey:UserDefaultGeneralData]];
    
    
    if(_dictFilter.count)
    {
        [_arrFilter addObjectsFromArray:@[@{@"title":CLocalize(@"range"),
                                            @"isExpand":[[_dictSelected valueForKey:@"range"] count] ? @1: @0 ,
                                            @"option":[[_dictFilter valueForKey:@"range"] count] ? [_dictFilter valueForKey:@"range"] :@"" },
                                          @{@"title":CLocalize(@"series"),
                                            @"isExpand":[[_dictSelected valueForKey:@"series"] count] ? @1: @0 ,
                                            @"option":[[_dictFilter valueForKey:@"series"] count] ? [_dictFilter valueForKey:@"series"] : @""},
                                          @{@"title":CLocalize(@"team"),
                                            @"isExpand":[[_dictSelected valueForKey:@"team"] count] ? @1: @0 ,
                                            @"option":[[_dictFilter valueForKey:@"team"] count] ? [_dictFilter valueForKey:@"team"] : @""},
                                          @{@"title":CLocalize(@"rarity"),
                                            @"isExpand":[[_dictSelected valueForKey:@"rarity"] count] ? @1: @0 ,
                                            @"option":[[_dictFilter valueForKey:@"rarity"] count] ? [_dictFilter valueForKey:@"rarity"] :@""}]];
    }
   
    
    
    [_tblFilter registerNib:[UINib nibWithNibName:@"MIFilterTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIFilterTableViewCell"];
    
    [_tblFilter registerNib:[UINib nibWithNibName:@"MIFilterSectionHeader" bundle:nil] forHeaderFooterViewReuseIdentifier:@"MIFilterSectionHeader"];
}

#pragma mark -
#pragma mark - Action Event

-(IBAction)btnApplyClicked:(UIButton*)sender
{
    if(self.FilterEventBlock)
        self.FilterEventBlock(_dictSelected);
}

-(IBAction)btnResetClicked:(UIButton*)sender
{
    [_arrFilter removeAllObjects];
    [_dictSelected removeAllObjects];
    if(_dictFilter.count)
    {
        [_arrFilter addObjectsFromArray:@[@{@"title":CLocalize(@"range"),
                                            @"isExpand":@0 ,
                                            @"option":[[_dictFilter valueForKey:@"range"] count] ? [_dictFilter valueForKey:@"range"] :@"" },
                                          @{@"title":CLocalize(@"series"),
                                            @"isExpand":@0 ,
                                            @"option":[[_dictFilter valueForKey:@"series"] count] ? [_dictFilter valueForKey:@"series"] : @""},
                                          @{@"title":CLocalize(@"team"),
                                            @"isExpand":@0 ,
                                            @"option":[[_dictFilter valueForKey:@"team"] count] ? [_dictFilter valueForKey:@"team"] : @""},
                                          @{@"title":CLocalize(@"rarity"),
                                            @"isExpand":@0 ,
                                            @"option":[[_dictFilter valueForKey:@"rarity"] count] ? [_dictFilter valueForKey:@"rarity"] :@""}]];
    };

    [_tblFilter reloadData];

}

#pragma mark -
#pragma mark - Functions
-(NSString *)strLocaliztionToNormal:(NSString *)strTitle
{
    if ([strTitle isEqualToString:CLocalize(@"range")]) {
        return @"range";
    }else if ([strTitle isEqualToString:CLocalize(@"series")]) {
        return @"series";
    }else if ([strTitle isEqualToString:CLocalize(@"team")]) {
        return @"team";
    }else{
        return @"rarity";
    }
}


#pragma mark -
#pragma mark - UITable view data source and delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _arrFilter.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *dict = _arrFilter[section];
    
    if ([dict booleanForKey:@"isExpand"])
    {
        if ([[dict valueForKey:@"option"] isKindOfClass:[NSArray class]])
        {
            return [[dict valueForKey:@"option"] count] ? [[dict valueForKey:@"option"] count] : 0;
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (IS_IPAD ? ((CScreenHeight*45)/490) : 45);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MIFilterTableViewCell";
    MIFilterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSDictionary *dict = _arrFilter[indexPath.section];
    NSDictionary *dictOption = [dict valueForKey:@"option"][indexPath.row];
    
    NSArray *arrSelected = [_dictSelected valueForKey:[self strLocaliztionToNormal:[dict stringValueForJSON:@"title"]]];
    
    [cell configureSelected:[arrSelected containsObject:[dictOption valueForKey:@"id"]]];
    [cell.lblTitle setText:[dictOption stringValueForJSON:@"name"]];
    
    [cell.btnClose touchUpInsideClicked:^{  
       
        NSMutableArray *tempArrSelected = [NSMutableArray new];
        [tempArrSelected addObjectsFromArray:arrSelected];
        [tempArrSelected removeObject:[dictOption valueForKey:@"id"]];
        [_dictSelected setValue:tempArrSelected forKey:[self strLocaliztionToNormal:[dict stringValueForJSON:@"title"]]];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (IS_IPAD ? ((CScreenHeight*55)/490) : 55);
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MIFilterSectionHeader *header = [_tblFilter dequeueReusableHeaderFooterViewWithIdentifier:@"MIFilterSectionHeader"];
    
     NSDictionary *dict = _arrFilter[section];
    
    [header.lblTitle setText:[[dict stringValueForJSON:@"title"] capitalizedString]];
    [header.btnDropDown setSelected:[dict booleanForKey:@"isExpand"]];
    
    [header.btnDropDown touchUpInsideClicked:^{
       
        header.btnDropDown.selected = !header.btnDropDown.selected;
        
        NSMutableDictionary *dictMut = [[NSMutableDictionary alloc] initWithDictionary:dict];
        
        [dictMut setValue:[NSNumber numberWithBool:header.btnDropDown.selected] forKey:@"isExpand"];
        [_arrFilter replaceObjectAtIndex:section withObject:dictMut];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        
    }];
    return header;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict = _arrFilter[indexPath.section];
    NSDictionary *dictOption = [dict valueForKey:@"option"][indexPath.row];
    
    NSArray *arrSelected = [_dictSelected valueForKey:[dict stringValueForJSON:@"title"]];
    
    if(![arrSelected containsObject:[dictOption valueForKey:@"id"]])
    {
        NSMutableArray *tempArrSelected = [NSMutableArray new];
        [tempArrSelected addObjectsFromArray:arrSelected];
        [tempArrSelected addObject:[dictOption valueForKey:@"id"]];
        [_dictSelected setValue:tempArrSelected forKey:[self strLocaliztionToNormal:[dict stringValueForJSON:@"title"]]];
    }
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    
}


@end

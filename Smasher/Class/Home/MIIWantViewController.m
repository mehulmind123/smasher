//
//  MIIWantViewController.m
//  Smasher
//
//  Created by Mac-00014 on 28/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIIWantViewController.h"
#import "MIMonsterCollectionViewCell.h"
#import "MIWantOwnHeaderView.h"
#import "MIFooterCollReusableView.h"
#import "MIFilterView.h"
#import "MICharacterDetailsViewController.h"
#import "MINoDataFoundCollectionViewCell.h"


@interface MIIWantViewController ()
{
    MIWantOwnHeaderView *header;
    MIFooterCollReusableView *footer;
    NSDictionary *dictFilter;
    NSMutableArray *arrChar;
    
    NSString *timeStamp;

    NSURLSessionDataTask *apiDataTask;
    UIRefreshControl *refreshControl;
}
@end

@implementation MIIWantViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    arrChar = [NSMutableArray new];
    timeStamp = @"0";
    
    [collView registerNib:[UINib nibWithNibName:@"MIMonsterCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIMonsterCollectionViewCell"];
    
    [collView registerNib:[UINib nibWithNibName:@"MINoDataFoundCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MINoDataFoundCollectionViewCell"];
    
     [collView registerNib:[UINib nibWithNibName:@"MIWantOwnHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    
    [collView registerNib:[UINib nibWithNibName:@"MIFooterCollReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
    
    
    [collView reloadData];
    
    [collView performBatchUpdates:nil completion:^(BOOL finished) {
        
        if(!arrChar.count && [timeStamp isEqualToString:@"0"])
            [self loadDataFromServer];
        
    }];
    
    //...UIRefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [collView addSubview:refreshControl];
    
}

#pragma mark -
#pragma mark - Load Data

- (void)pullToRefresh
{
    timeStamp = @"0";
    [self loadDataFromServer];
}

- (void)loadDataFromServer
{
    __block MINoDataFoundCollectionViewCell *cell;
    if(!arrChar.count && [timeStamp isEqualToString:@"0"] && !refreshControl.refreshing)
    {
        cell  = (MINoDataFoundCollectionViewCell*)[collView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        if(cell.vWContent)
        {
            [self removeNodataFoundInView:cell.vWContent];
            [self startLoadingAnimationInView:cell.vWContent];
            
        }
    }
    
    if (apiDataTask && apiDataTask.state == NSURLSessionTaskStateRunning)
        [apiDataTask cancel];

    apiDataTask = [[APIRequest request] loadCharcterListWithType:CCharStatusWant searchString:header.txtSearch.text andRange:[dictFilter valueForKey:@"range"] andSeries:[dictFilter valueForKey:@"series"] andTeam:[dictFilter valueForKey:@"team"] andRarity:[dictFilter valueForKey:@"rarity"] andTimestamp:timeStamp  completed:^(id responseObject, NSError *error)
                   {
                        [refreshControl endRefreshing];
                       
                       if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                       {
                           if([timeStamp isEqualToString:@"0"])
                           {
                               [arrChar removeAllObjects];
                           }
                           
                           if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                           {
                               [arrChar addObjectsFromArray:[responseObject valueForKey:CJsonData]];
                               if (arrChar.count)
                                   timeStamp = [[arrChar lastObject] stringValueForJSON:@"updated_at"];
                           }
                           else
                               timeStamp  = @"0";
                           
                       }
                       
                       [collView reloadData];
                       
                       if(!arrChar.count)
                       {

                           [self stopLoadingAnimationInView:cell.vWContent type:error ? StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound  message:(header.txtSearch.text.isBlankValidationPassed || dictFilter.count) ? CLocalize(CMessageSearchNoResultFound) : @"" touchUpInsideClickedEvent:^{
                               
                               [self loadDataFromServer];
                               
                           }];
                           
                           if(!cell.vWContent)
                           {
                               [collView performBatchUpdates:nil completion:^(BOOL finished) {
                                   
                                   cell  = (MINoDataFoundCollectionViewCell*)[collView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                                   if(cell.vWContent)
                                       [self showNoDataFoundInView:cell.vWContent message:(header.txtSearch.text.isBlankValidationPassed || dictFilter.count) ? CLocalize(CMessageSearchNoResultFound) : @""];
                               }];
                               
                           }
                       }
                       else
                       {
                           [self stopLoadingAnimationInView:cell.vWContent type:StopAnimationTypeRemove message:@"" touchUpInsideClickedEvent:nil];
                       }
                       
                   }];
}

- (void)changeCharacterStatus:(NSUInteger)status indexPath:(NSIndexPath*)indexPath
{
        [[APIRequest request] changeCharacterStatusWithCharId:arrChar[indexPath.row][@"character_id"] withStatus:status completed:^(id responseObject, NSError *error)
         {
             if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
             {
                 if(arrChar.count>indexPath.row)
                  [arrChar removeObjectAtIndex:indexPath.row];
                 
                 [collView reloadData];
                 
                 
                 if(!arrChar.count)
                 {
                     [collView performBatchUpdates:nil completion:^(BOOL finished) {
                         
                         MINoDataFoundCollectionViewCell *cell  = (MINoDataFoundCollectionViewCell*)[collView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                         if(cell.vWContent)
                             [self showNoDataFoundInView:cell.vWContent message:@""];
                     }];
                 }
             }
             else
                 [collView reloadItemsAtIndexPaths:@[indexPath]];
         }];

    
}

#pragma mark -
#pragma mark - UICollectionView DataSoure and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrChar.count? arrChar.count : 1;;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(!arrChar.count)
    {
        if (IS_IPAD)
            return  CGSizeMake(CScreenWidth, CScreenWidth*0.67);
        else
            return  CGSizeMake(CScreenWidth, CScreenWidth*1.09);
    }
    
    return  CGSizeMake(((CScreenWidth*(IS_IPAD ? 172 : 161))/375)-(CDeviceHeight <= 568 ? 4 :0), (CScreenWidth*(IS_IPAD ? 256 : 244))/375);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    if(arrChar.count)
    {
        static NSString *identifier = @"MIMonsterCollectionViewCell";
        
        MIMonsterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
         [cell configureData:arrChar[indexPath.row] indexPath:indexPath];
        
        [cell.btnIOwn touchUpInsideClicked:^{
            
            [cell btnSwitchClicked:cell.btnIOwn];
            [self changeCharacterStatus:CCharStatusOwn indexPath:indexPath];
            
        }];
        [cell.btnIWant touchUpInsideClicked:^{
            
            [cell btnSwitchClicked:cell.btnIWant];
            [self changeCharacterStatus:CCharStatusNone indexPath:indexPath];
        }];
        
        if(indexPath.row == arrChar.count - 1 && ![timeStamp isEqualToString:@"0"])
            [self loadDataFromServer];
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"MINoDataFoundCollectionViewCell";
        
        MINoDataFoundCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        return cell;
        
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(arrChar.count)
    {
        MICharacterDetailsViewController *detailsVC = [[MICharacterDetailsViewController alloc] initWithNibName:@"MICharacterDetailsViewController" bundle:nil];
       [detailsVC setIObject:arrChar[indexPath.row][@"character_id"]];
       [detailsVC setBlock:^(NSNumber *status, NSError *error)
         {
             if([status isEqual:[NSNumber numberWithInteger:CCharStatusWant]])
             {
                 timeStamp = @"0";
                 [self loadDataFromServer];
             }
             else
             {
                 [arrChar removeObjectAtIndex:indexPath.row];
                 [collView reloadData];
                 
                 if(!arrChar.count)
                 {
                     [collView performBatchUpdates:nil completion:^(BOOL finished) {
                         
                         MINoDataFoundCollectionViewCell *cell  = (MINoDataFoundCollectionViewCell*)[collView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                         if(cell.vWContent)
                             [self showNoDataFoundInView:cell.vWContent message:@""];
                     }];
                 }

             }
             
         }];
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:detailsVC] animated:YES completion:nil];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(CScreenWidth, (CScreenWidth * 180) / 375);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    if(arrChar.count > 2 || !arrChar.count)
        return CGSizeMake(CScreenWidth, (CScreenWidth * 64) / 375);
    else
        return CGSizeMake(CScreenWidth, (CScreenWidth * 227) / 375);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader)
    {
        header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        
        [header configure];
        [header.lblTitle setText:[CLocalize(@"I WANT") uppercaseString]];
        
        NSString *strTotalSmash = [CLocalize(@"TOTAL SMASH POINTS YOU WANT :") uppercaseString];
        
        if(appDelegate.loginUser.total_want)
            [header.lblSmashPoint setText:[NSString stringWithFormat:@"%@ %lld", strTotalSmash,appDelegate.loginUser.total_want]];
        else
            [header.lblSmashPoint setText:[NSString stringWithFormat:@"%@ 00", strTotalSmash]];
            
        
   
        [header.txtSearch setDelegate:self];
        [header.btnFilter touchUpInsideClicked:^{
            
            __block MIFilterView *filter = [[MIFilterView alloc] initWithSelectedOption:dictFilter handler:^(NSDictionary *dictSelected)
                                            {
                                                [self dismissPopUp:filter];
                                                dictFilter = dictSelected;
                                                timeStamp = @"0";
                                                [self loadDataFromServer];
                                                
                                            }];
            
            [self.navigationController presentPopUp:filter from:PresentTypeBottom shouldCloseOnTouchOutside:YES];
            
        }];
        
        [header.btnSearch touchUpInsideClicked:^{
            
            [self resignKeyboard];
            timeStamp = @"0";
            [self loadDataFromServer];
        }];
        return  header;
    }
    else if (kind == UICollectionElementKindSectionFooter)
    {
        footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:indexPath];
        
        return footer;
    }
    
    return nil;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
      [self.navigationController.navigationBar setHidden:scrollView.contentOffset.y >= 64];
      [UIApplication sharedApplication].statusBarHidden = scrollView.contentOffset.y >= 64;
}

#pragma mark -
#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
     timeStamp = @"0";
    [self loadDataFromServer];
    
    return YES;
}


@end

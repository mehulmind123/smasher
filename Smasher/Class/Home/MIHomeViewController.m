//
//  MIHomeViewController.m
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIHomeViewController.h"
#import "MIRightPanelViewController.h"
#import "MICharacterDetailsViewController.h"
#import "MIMonsterCollectionViewCell.h"
#import "MINoDataFoundCollectionViewCell.h"
#import "MIHomeHeaderView.h"
#import "MIFooterCollReusableView.h"
#import "MIFilterView.h"
#import "MIIWantViewController.h"
#import "MIIOwnViewController.h"

@interface MIHomeViewController ()
{
    MIHomeHeaderView *header;
    MIFooterCollReusableView *footer;
    
    NSDictionary *dictFilter;
    
    NSMutableArray *arrChar;
    NSInteger page;
    NSURLSessionDataTask *apiDataTask;
    UIRefreshControl *refreshControl;
}

@end

@implementation MIHomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     //[self.navigationController.navigationBar setTintColor:collView.contentOffset.y >= ((CScreenWidth * 157) / 375) ? ColorBlue_6BCBF3 : ColorWhite_FFFFFF];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    arrChar = [NSMutableArray new];
    page = 1;
    
    if(self.iObject)
    {
        [arrChar addObjectsFromArray:[self.iObject valueForKey:@""]];
        page = [self.iObject integerForKey:@""];
    }
    
    [collView registerNib:[UINib nibWithNibName:@"MIMonsterCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIMonsterCollectionViewCell"];

    [collView registerNib:[UINib nibWithNibName:@"MINoDataFoundCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MINoDataFoundCollectionViewCell"];


    [collView registerNib:[UINib nibWithNibName:@"MIHomeHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];

    [collView registerNib:[UINib nibWithNibName:@"MIFooterCollReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];

    //...UIRefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [collView addSubview:refreshControl];

    [collView reloadData];
    
    if(!arrChar.count && page == 1)
       [self performSelector:@selector(loadDataFromServer) withObject:nil afterDelay:0.1];
    
}


#pragma mark -
#pragma mark - Load Data
- (void)pullToRefresh
{
    page = 1;
    [self loadDataFromServer];
}
- (void)loadDataFromServer
{
   __block MINoDataFoundCollectionViewCell *cell;
    if(!arrChar.count && page == 1 && !refreshControl.refreshing)
    {
        cell  = (MINoDataFoundCollectionViewCell*)[collView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        if(cell.vWContent)
        {
            [self removeNodataFoundInView:cell.vWContent];
            [self startLoadingAnimationInView:cell.vWContent];
            
        }
    }
    
    if (apiDataTask && apiDataTask.state == NSURLSessionTaskStateRunning)
        [apiDataTask cancel];
    
    
    apiDataTask = [[APIRequest request] loadCharcterList:header.txtSearch.text andRange:[dictFilter valueForKey:@"range"] andSeries:[dictFilter valueForKey:@"series"] andTeam:[dictFilter valueForKey:@"team"] andRarity:[dictFilter valueForKey:@"rarity"] andOffset:page completed:^(id responseObject, NSError *error)
    {
        
        [refreshControl endRefreshing];
        if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            if(page == 1)
            {
                [arrChar removeAllObjects];
            }
            
            [arrChar addObjectsFromArray:[responseObject valueForKey:CJsonData]];
            page = [[APIRequest request] paginationWithResponse:responseObject];
        }
        
        [collView reloadData];
        if(!arrChar.count)
        {
            [self stopLoadingAnimationInView:cell.vWContent type:error ? StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound  message:(header.txtSearch.text.isBlankValidationPassed || dictFilter.count) ? CLocalize(CMessageSearchNoResultFound) : @"" touchUpInsideClickedEvent:^{
                
                [self loadDataFromServer];
                
            }];
            
            if(!cell.vWContent)
            {
                [collView performBatchUpdates:nil completion:^(BOOL finished) {

                    cell  = (MINoDataFoundCollectionViewCell*)[collView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    if(cell.vWContent)
                        [self showNoDataFoundInView:cell.vWContent message:(header.txtSearch.text.isBlankValidationPassed || dictFilter.count) ? CLocalize(CMessageSearchNoResultFound):@""];
                }];
                
            }
        }
        else
        {
            [self stopLoadingAnimationInView:cell.vWContent type:StopAnimationTypeRemove message:@""  touchUpInsideClickedEvent:nil];
        }
        
    }];
}



#pragma mark -
#pragma mark - UICollectionView DataSoure and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrChar.count? arrChar.count : 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(!arrChar.count)
    {
        if (IS_IPAD)
            return  CGSizeMake(CScreenWidth, (CScreenWidth*180)/375);
        else
            return  CGSizeMake(CScreenWidth, (CScreenWidth*337)/375);
    }
    
    
    return  CGSizeMake(((CScreenWidth*(IS_IPAD ? 172 : 161))/375)-(CDeviceHeight <= 568 ? 4 :0), (CScreenWidth*(IS_IPAD ? 256 : 244))/375);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(arrChar.count)
    {
        static NSString *identifier = @"MIMonsterCollectionViewCell";
        
        MIMonsterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
         [cell configureData:arrChar[indexPath.row] indexPath:indexPath];
        
        [cell.btnIOwn touchUpInsideClicked:^{
         
            [self configureCell:cell sender:cell.btnIOwn indexPath:indexPath];
        }];
        [cell.btnIWant touchUpInsideClicked:^{
            
           [self configureCell:cell sender:cell.btnIWant indexPath:indexPath];
        }];
        
        if( indexPath.row == arrChar.count - 1 &&  page > 1)
        {
            [self loadDataFromServer];
        }
        return cell;
    }
    else
    {
        static NSString *identifier = @"MINoDataFoundCollectionViewCell";
        
        MINoDataFoundCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        return cell;

    }
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(arrChar.count)
    {
        MICharacterDetailsViewController *detailsVC = [[MICharacterDetailsViewController alloc] initWithNibName:@"MICharacterDetailsViewController" bundle:nil];
        [detailsVC setIObject:arrChar[indexPath.row][@"character_id"]];
        [detailsVC setBlock:^(NSNumber *status, NSError *error)
        {
            if([@[@0,@1,@2] containsObject:status])
            {
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict addEntriesFromDictionary:arrChar[indexPath.row]];
                [dict setValue:status forKey:@"character_status"];
                [arrChar replaceObjectAtIndex:indexPath.row withObject:dict];
                [collView reloadData];
                
            }
            
            
        }];
        [self presentViewController:[UINavigationController navigationControllerWithRootViewController:detailsVC] animated:YES completion:nil];
        
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(CScreenWidth, (CScreenWidth * 250) / 375);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{

    if(arrChar.count > 2 || !arrChar.count)
        return CGSizeMake(CScreenWidth, (CScreenWidth * 64) / 375);
    else
        return CGSizeMake(CScreenWidth, (CScreenWidth * 157) / 375);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader)
    {
        header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        [header configure];
        
        [header.txtSearch setDelegate:self];
        
        if(appDelegate.loginUser)
        {
            header.lblUserName.text = [NSString stringWithFormat:@"%@ %@", CLocalize(@"Welcome"),appDelegate.loginUser.user_name];
            
            if(appDelegate.loginUser.total_own)
                header.lblIOwn.text = [NSString stringWithFormat:@"%lld",appDelegate.loginUser.total_own];
            else
                header.lblIOwn.text = @"00";
            
            if(appDelegate.loginUser.total_want)
                header.lblIWant.text = [NSString stringWithFormat:@"%lld",appDelegate.loginUser.total_want];
            else
                header.lblIWant.text = @"00";
        }
        else
        {
            header.lblIOwn.text = @"00";
            header.lblIWant.text = @"00";
 
        }
      
        [header.btnIOwn touchUpInsideClicked:^{
            
            if(!appDelegate.loginUser)
            {
                [appDelegate showLoginRequirePopUp:self];
            }
            else
            {
                MIIOwnViewController *iOwnVC = [[MIIOwnViewController alloc] initWithNibName:@"MIIOwnViewController" bundle:nil];
                
                [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:iOwnVC]];
            }

            
        }];
        
        [header.btnIWant touchUpInsideClicked:^{
            
            if(!appDelegate.loginUser)
            {
                [appDelegate showLoginRequirePopUp:self];
            }
            else
            {
                MIIWantViewController *iWantVC = [[MIIWantViewController alloc] initWithNibName:@"MIIWantViewController" bundle:nil];
                
                [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:iWantVC]];
            }
        }];
        
        [header.btnFilter touchUpInsideClicked:^{
            
            __block MIFilterView *filter = [[MIFilterView alloc] initWithSelectedOption:dictFilter handler:^(NSDictionary *dictSelected)
            {
                [self dismissPopUp:filter];
                dictFilter = dictSelected;
                page = 1;
                [self loadDataFromServer];
            
            }];
            
            [self.navigationController presentPopUp:filter from:PresentTypeBottom shouldCloseOnTouchOutside:YES];
            
        }];
        [header.btnSearch touchUpInsideClicked:^{
            
            [self resignKeyboard];
            page = 1;
            [self loadDataFromServer];
            
        }];
        return  header;
    }
    else if (kind == UICollectionElementKindSectionFooter)
    {
        footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:indexPath];
        return footer;
    }

    return nil;
}

- (void)configureCell:(MIMonsterCollectionViewCell*)cell sender:(UIButton*)sender indexPath:(NSIndexPath*)indexPath
{
    if(!appDelegate.loginUser)
    {
        [appDelegate showLoginRequirePopUp:self];
    }
    else
    {
        if(sender.selected)
        {
            [self changeCharacterStatus:CCharStatusNone indexPath:indexPath];
        }
        else if(sender == cell.btnIWant)
        {
            [self changeCharacterStatus:CCharStatusWant indexPath:indexPath];
        }
        else if (sender == cell.btnIOwn)
        {
            [self changeCharacterStatus:CCharStatusOwn indexPath:indexPath];
        }
        
        [cell btnSwitchClicked:sender];
        
    }
}

- (void)changeCharacterStatus:(NSUInteger)status indexPath:(NSIndexPath*)indexPath
{
    [[APIRequest request] changeCharacterStatusWithCharId:arrChar[indexPath.row][@"character_id"] withStatus:status completed:^(id responseObject, NSError *error)
     {
         if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             
             if(appDelegate.loginUser.total_own)
                 header.lblIOwn.text = [NSString stringWithFormat:@"%lld",appDelegate.loginUser.total_own];
             else
                 header.lblIOwn.text = @"00";
             
             if(appDelegate.loginUser.total_want)
                 header.lblIWant.text = [NSString stringWithFormat:@"%lld",appDelegate.loginUser.total_want];
             else
                 header.lblIWant.text = @"00";
             
             
             NSMutableDictionary *dict = [NSMutableDictionary new];
             [dict addEntriesFromDictionary:arrChar[indexPath.row]];
             [dict setValue:[NSNumber numberWithInteger:status] forKey:@"character_status"];
             [arrChar replaceObjectAtIndex:indexPath.row withObject:dict];
             [collView reloadItemsAtIndexPaths:@[indexPath]];
             
         }
         else
             [collView reloadItemsAtIndexPaths:@[indexPath]];
     }];
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    [self.navigationController.navigationBar setHidden:scrollView.contentOffset.y >= 64];
    [UIApplication sharedApplication].statusBarHidden = scrollView.contentOffset.y >= 64;
    
}
#pragma mark -
#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    page = 1;
    [self loadDataFromServer];
    
    return YES;
}

@end

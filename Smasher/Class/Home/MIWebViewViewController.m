//
//  MIWebViewViewController.m
//  Smasher
//
//  Created by Mac-00014 on 28/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIWebViewViewController.h"
#import "MIFooterView.h"
@interface MIWebViewViewController ()<UITextViewDelegate>
{
    MIFooterView *footer;
    NSDictionary *dictInfo;
}
@end

@implementation MIWebViewViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        footer = [MIFooterView footer];
        [vWFooter addSubview:footer];
    });
    
    lblHeader.text = [CLocalize(@"SMASH + CHARACTER = SMASH POINTS!") uppercaseString];
    [lblHeader hideByHeight:true];
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    //    [self loadDataFromLocal];
    //    [self loadDataFromServer];
    
    switch (self.GeneralDataType)
    {
            case GeneralDataTypeAboutUs:
            [self aboutUS];
            break;
            
            case GeneralDataTypeAboutSmashPoint:
            [lblHeader hideByHeight:false];
            [self aboutSmashPoint];
            break;
            
            case GeneralDataTypePrivacyPolicy:
            
            [self privacyPolicy];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - Load Data

/*
 - (void)loadDataFromServer
 {
 [UIApplication sharedApplication].statusBarHidden = NO;
 [self startLoadingAnimationInView:txtView];
 
 [[APIRequest request] loadCMS:self.GeneralDataType completed:^(id responseObject, NSError *error)
 {
 if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
 {
 [self loadDataFromLocal];
 }
 
 [self stopLoadingAnimationInView:txtView type:[dictInfo count] ? StopAnimationTypeRemove : error ? StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound message:@""  touchUpInsideClickedEvent:^{
 
 [self loadDataFromServer];
 }];
 }];
 }
 
 - (void)loadDataFromLocal
 {
 dictInfo = [CUserDefaults valueForKey : self.GeneralDataType == GeneralDataTypeAboutSmashPoint ?
 UserDefaultCMSAboutSmashPoint  : self.GeneralDataType == GeneralDataTypeAboutUs ?
 UserDefaultCMSAboutUs  : self.GeneralDataType == GeneralDataTypeContactUs ?
 UserDefaultCMSContactUs  : UserDefaultCMSPrivacyPolicy];
 
 if([dictInfo count])
 {
 //self.title   = [dictInfo stringValueForJSON:@"title"];
 txtView.text = [dictInfo stringValueForJSON:@"descriptions"];
 }
 }*/

- (void)aboutSmashPoint
{
    
    txtView.delegate = self;
    NSString *smashPoint = [NSString stringWithFormat:@"\n%@\n\n%@\n\n%@", CLocalize(@"Each character is worth a number of Smash Points, so add up your collection using the Collector’s Guide to find your Smash Point score. Download the Collector’s Guide at smashersworld.com and tally up your score now!"), CLocalize(@"Want to become the ultimate Smashers champion? Challenge your friends in the Smash Arena where the smashing never ends! Download the Smashers Game Board at smashersworld.com and take your smashing power to the next level. Earn more Smash Points to claim your title as a Smashers elite!"), CLocalize(@"Smash, collect and trade to grow your collection and Smash Points!")];
//    NSString *smashPoint = @"\nEach character is worth a number of Smash Points, so add up your collection using the Collector’s Guide to find your Smash Point score. Download the Collector’s Guide at smashersworld.com and tally up your score now! \n\nWant to become the ultimate Smashers champion? Challenge your friends in the Smash Arena where the smashing never ends! Download the Smashers Game Board at smashersworld.com and take your smashing power to the next level. Earn more Smash Points to claim your title as a Smashers elite!\n\nSmash, collect and trade to grow your collection and Smash Points!";
    
    NSMutableAttributedString *mutString = [[NSMutableAttributedString alloc] initWithString:smashPoint];
    
    [mutString setAttributes:@{NSForegroundColorAttributeName:ColorBlack_000000,
                               NSFontAttributeName:CFontGroBold(IS_IPAD ? 19 : 15)} range:NSMakeRange(0, smashPoint.length)];
    
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"smashersworld.com" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSRange range = NSMakeRange(0, mutString.length);
    
    if (!error) {
        [regex enumerateMatchesInString:smashPoint options:NSMatchingReportCompletion range:range usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
         {
             [mutString addAttributes:@{NSLinkAttributeName:@"http://www.smashersworld.com",
                                        NSFontAttributeName:CFontGroBold(IS_IPAD ? 19 : 15),
                                        NSForegroundColorAttributeName:[UIColor blueColor],
                                        } range:result.range];
             
         }];
    }
    
    txtView.attributedText = mutString;
    
    
}

- (void)aboutUS
{
    txtView.text = CLocalize(@"ZURU is a disruptive and award-winning company that designs, develops, manufactures and markets innovative toys. Inspired by kids and imaginative play, ZURU is one of the fastest growing toy companies and is known for their agility, creativity and new-age manufacturing techniques. The company employs more than 400 staff, has 10 offices and supplies most major retailers in 120+ countries. ZURU has delighted millions of families all over the world through partnerships with entertainment properties, including Nickelodeon, Disney, Universal Studios and DreamWorks as well as successfully building their own global brands such as Bunch O Balloons™, X-Shot™, Robo Alive™, Micro Boats™ and Hamsters in a House™. Let’s reimagine play, everyday!");
    
//    txtView.text = @"ZURU is a disruptive and award-winning company that designs, develops, manufactures and markets innovative toys. Inspired by kids and imaginative play, ZURU is one of the fastest growing toy companies and is known for their agility, creativity and new-age manufacturing techniques. The company employs more than 400 staff, has 10 offices and supplies most major retailers in 120+ countries. ZURU has delighted millions of families all over the world through partnerships with entertainment propertiesk, including Nickelodeon, Disney, Universal Studios and DreamWorks as well as successfully building their own global brands such as Bunch O Balloons™, X-Shot™, Robo Alive™, Micro Boats™ and Hamsters in a House™. Let’s reimagine play, everyday!";
}

- (void)privacyPolicy
{
    
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithTextAttachment:(NSTextAttachment *)textAttachment inRange:(NSRange)characterRange
{
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    [[UIApplication sharedApplication] openURL:URL];
    return NO;
}

// ... Availble IOS 10
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction
{
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithTextAttachment:(NSTextAttachment *)textAttachment inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction
{
    return YES;
}
@end

//
//  MIInAppWebViewController.m
//  Smasher
//
//  Created by Mac-00014 on 03/10/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIInAppWebViewController.h"
#import <WebKit/WebKit.h>

@interface MIInAppWebViewController ()<WKUIDelegate,WKNavigationDelegate>
{
    WKWebView *webView;
}
@end

@implementation MIInAppWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initliaze];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark - General Method

-(void)initliaze
{
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    if (!_isHideBack)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_back"] style:UIBarButtonItemStylePlain target:self action:@selector(barBtnDoneClicked:)];
    }
    
   
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.iObject cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    theConfiguration.allowsInlineMediaPlayback = YES;

    webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, CScreenHeight-64) configuration:theConfiguration];
    webView.navigationDelegate = self;
    webView.translatesAutoresizingMaskIntoConstraints = NO;
    webView.allowsBackForwardNavigationGestures = YES;
    [webView loadRequest:request];
    [self.view addSubview:webView];
    
    [self startLoadingAnimationInView:self.view];
}

#pragma mark -
#pragma mark - Action Event

- (IBAction)barBtnDoneClicked:(UIBarButtonItem*)sender
{
    if([webView canGoBack])
    {
        [webView goBack];
    }
    else
     [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark -
#pragma mark - WKWebViewDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    
//    if (navigationAction.navigationType == WKNavigationTypeLinkActivated)
//            decisionHandler(WKNavigationActionPolicyCancel);
//    else
        decisionHandler(WKNavigationActionPolicyAllow);
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    [self stopLoadingAnimationInView:self.view type:StopAnimationTypeRemove message:@"" touchUpInsideClickedEvent:nil];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    [self stopLoadingAnimationInView:self.view type:StopAnimationTypeRemove message:@"" touchUpInsideClickedEvent:nil];
}

@end

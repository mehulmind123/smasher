//
//  MIWebViewViewController.h
//  Smasher
//
//  Created by Mac-00014 on 28/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"
typedef NS_ENUM(NSInteger, GeneralDataType)
{
    GeneralDataTypeAboutSmashPoint = 1,
    GeneralDataTypeAboutUs = 2,
    GeneralDataTypeContactUs = 3 ,
    GeneralDataTypePrivacyPolicy = 4,
    
};
@interface MIWebViewViewController : MIParentViewController
{
    IBOutlet UITextView *txtView;
    IBOutlet UIView *vWFooter;
    IBOutlet UILabel *lblHeader;
}
@property (nonatomic, assign) GeneralDataType  GeneralDataType;
@end

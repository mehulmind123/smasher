//
//  MIRightPanelViewController.m
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIRightPanelViewController.h"
#import "MISideMenuTableViewCell.h"
#import "MIHomeViewController.h"
#import "MIIWantViewController.h"
#import "MIIOwnViewController.h"
#import "MIWebViewViewController.h"
#import "MIChangePasswordViewController.h"
#import "MILoginViewController.h"
#import "MIRegisterViewController.h"
#import "MIEditProfileViewController.h"
#import "MIInAppWebViewController.h"
#import "MIChangeLanguageViewController.h"

@interface MIRightPanelViewController ()
{
    NSArray *arrSideMenu;
}
@end

@implementation MIRightPanelViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    [self refreshData];
    //.....RefreshData of UITableView on notification fired.....
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:NotificationLeftPanelWillShow object:nil];
    [tblSideMenu registerNib:[UINib nibWithNibName:@"MISideMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISideMenuTableViewCell"];
    
    
}

- (void)refreshData
{
    if(appDelegate.loginUser)
    {
        arrSideMenu = @[@"Home",
                        @"I Own",
                        @"I Want",
                        @"Change Password",
                        @"Change Language",
                        @"About Smash Points",
                        @"About Us",
                        @"Contact Us",
                        @"Privacy Policy",
                        @"Children's Privacy Policy",
                        @"Terms and Conditions",
                        @"Visit Smashersworld.com",
                        @"Logout"];
        
        [self showHeaderView:YES];
        
        lblUserName.text = appDelegate.loginUser.user_name;
        
        NSString *strWant = [CLocalize(@"I WANT") uppercaseString];
        if(appDelegate.loginUser.total_want)
            [btnIWant setTitle:[NSString stringWithFormat:@"%@ : %lld", strWant, appDelegate.loginUser.total_want] forState:UIControlStateNormal];
        else
            [btnIWant setTitle:[NSString stringWithFormat:@"%@ : 00", strWant] forState:UIControlStateNormal];
        
        
        NSString *strOwn = [CLocalize(@"I OWN") uppercaseString];
        if(appDelegate.loginUser.total_own)
            [btnIOwn setTitle:[NSString stringWithFormat:@"%@ : %lld", strOwn, appDelegate.loginUser.total_own ] forState:UIControlStateNormal];
        else
            [btnIOwn setTitle:[NSString stringWithFormat:@"%@ : 00", strOwn] forState:UIControlStateNormal];
    }
    else
    {
        arrSideMenu = @[@"Home",
                        @"Create an Account",
                        @"Login",
                        @"Change Language",
                        @"About Smash Points",
                        @"About Us",
                        @"Contact Us",
                        @"Privacy Policy",
                        @"Children's Privacy Policy",
                        @"Terms and Conditions",
                        @"Visit Smashersworld.com"];
    
        [self showHeaderView:NO];
    }
    [tblSideMenu reloadData];
}
- (void)showHeaderView:(BOOL)show
{
    [vWIOwnAndIWant hideByHeight:!show];
    [btnEditProfile setHidden:!show];
    [lblUserName setHidden:!show];
    [lblYouSmashPoint setHidden:!show];
    [imgVLogo setHidden:show];

}
#pragma mark -
#pragma mark - Action Event

-(IBAction)btnEditProfileClicked:(UIButton*)sender
{    
    [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:^{
        
        MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
        [(UINavigationController*)appDelegate.sidePanelViewController.rootViewController pushViewController:editProfileVC animated:YES];
    }];
}

-(IBAction)btnIOwnClicked:(UIButton*)sender
{
    UINavigationController *nav = (UINavigationController*)appDelegate.sidePanelViewController.rootViewController;
    if(![[nav.viewControllers firstObject] isKindOfClass:[MIIOwnViewController class]])
    {
        MIIOwnViewController *iOwnVC = [[MIIOwnViewController alloc] initWithNibName:@"MIIOwnViewController" bundle:nil];
        
        [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:iOwnVC]];
    }
    
    [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:nil];
    
}

-(IBAction)btnIWantClicked:(UIButton*)sender
{
    UINavigationController *nav = (UINavigationController*)appDelegate.sidePanelViewController.rootViewController;
    
    if(![[nav.viewControllers firstObject] isKindOfClass:[MIIWantViewController class]])
    {
        MIIWantViewController *iWantVC = [[MIIWantViewController alloc] initWithNibName:@"MIIWantViewController" bundle:nil];
        
        [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:iWantVC]];
        
    }
    [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:nil];
}

#pragma mark -
#pragma mark - UITable view data source and delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSideMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return IS_IPAD ? (CScreenWidth*40)/375 : (CScreenWidth*50)/375;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MISideMenuTableViewCell";
    MISideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [cell.lblTitle setText:CLocalize(arrSideMenu[indexPath.row])];
    
    cell.lblTitle.textColor = [arrSideMenu[indexPath.row] isEqualToString:@"Logout"] ? ColorRed_E93429 : ColorBlue_6BCBF3;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UINavigationController *nav = (UINavigationController*)appDelegate.sidePanelViewController.rootViewController;

    
    if(appDelegate.loginUser)
    {
        
        switch (indexPath.row)
        {
            case 0:
            {
                if(![[nav.viewControllers firstObject] isKindOfClass:[MIHomeViewController class]])
                {
                    MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                    
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:homeVC]];
                }
                
                
                break;
            }
            case 1:
            {
                
                if(![[nav.viewControllers firstObject] isKindOfClass:[MIIOwnViewController class]])
                {
                    MIIOwnViewController *iOwnVC = [[MIIOwnViewController alloc] initWithNibName:@"MIIOwnViewController" bundle:nil];
                    
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:iOwnVC]];
                }
                
                
                break;
            }
            case 2:
            {
                if(![[nav.viewControllers firstObject] isKindOfClass:[MIIWantViewController class]])
                {
                    MIIWantViewController *iWantVC = [[MIIWantViewController alloc] initWithNibName:@"MIIWantViewController" bundle:nil];
                    
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:iWantVC]];
                    
                }
                break;
            }
            case 3:
            {
                if(![[nav.viewControllers firstObject] isKindOfClass:[MIChangePasswordViewController class]])
                {
                    MIChangePasswordViewController *iWantVC = [[MIChangePasswordViewController alloc] initWithNibName:@"MIChangePasswordViewController" bundle:nil];
                    
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:iWantVC]];
                }

                break;
            }
            case 4:
            {
                if(![[nav.viewControllers firstObject] isKindOfClass:[MIChangeLanguageViewController class]])
                {
                    MIChangeLanguageViewController *languageVC = [[MIChangeLanguageViewController alloc] initWithNibName:@"MIChangeLanguageViewController" bundle:nil];
                    languageVC.view.tag = 100;
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:languageVC]];
                    
                }
                break;
            }
            case 5:
            {
               if(![[nav.viewControllers firstObject].title isEqualToString:arrSideMenu[indexPath.row]])
                {
                    MIWebViewViewController *webView = [[MIWebViewViewController alloc] initWithNibName:@"MIWebViewViewController" bundle:nil];
                    webView.title = CLocalize(arrSideMenu[indexPath.row]);
                    [webView setGeneralDataType:GeneralDataTypeAboutSmashPoint];
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:webView]];
                    
                }
                break;
            }
            case 6:
            {
               if(![[nav.viewControllers firstObject].title isEqualToString:arrSideMenu[indexPath.row]])
                {
                    MIWebViewViewController *webView = [[MIWebViewViewController alloc] initWithNibName:@"MIWebViewViewController" bundle:nil];
                    webView.title = CLocalize(arrSideMenu[indexPath.row]);
                    [webView setGeneralDataType:GeneralDataTypeAboutUs];
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:webView]];
                }
                break;
            }
            case 7:
            {
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL URLWithString:CFooterContactUsUrl];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
            
                break;
            }
            case 8:
            {
               NSString *path = [[NSBundle mainBundle] pathForResource:@"ZURU_Privacy_Policy" ofType:@"pdf"];
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL fileURLWithPath:path];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                break;
            }
            case 9:
            {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"Childrens_Privacy_Policy" ofType:@"pdf"];
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL fileURLWithPath:path];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                break;
            }
            case 10:
            {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"Terms_Conditions" ofType:@"pdf"];
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL fileURLWithPath:path];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                break;
            }
            case 11:
            {
//                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
//                inAppWebVC.iObject = [NSURL URLWithString:CSmasherWorld_Com];
//                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                [self openURLInSafari:CSmasherWorld_Com];
                
                break;
            }
            case 12:
            {
                
                dispatch_async(GCDMainThread, ^{
                    
                    [CustomAlertView iOSAlertWithTwoButton:@"" withMessage:CLocalize(CMessageLogout) firstButtonTitle:CLocalize(CYes) secondButtonTitle:CLocalize(CNo) onView:self handler:^(UIAlertAction *action)
                     {
                         if([action.title isEqualToString:CLocalize(CYes)])
                         {
                             [appDelegate logoutUserFromSideMenu];
                         }
                         
                     }];
                });
                
                break;
            }
                
        }

        if(indexPath.row != 12)
            [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:nil];
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
            {
                if(![[nav.viewControllers firstObject]  isKindOfClass:[MIHomeViewController class]])
                {
                    MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
                    
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:homeVC]];
                }
    
                break;
            }
            case 1:
            {
                
                [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:^{
                    UINavigationController *nav = (UINavigationController*)appDelegate.sidePanelViewController.rootViewController;
                    
                    [appDelegate showSelectDOBPopUp:[nav.viewControllers objectAtIndex:0]];
                    
                }];
                break;
            }
            case 2:
            {
                [appDelegate logoutUser];
                break;
            }
            case 3:
            {
                if(![[nav.viewControllers firstObject] isKindOfClass:[MIChangeLanguageViewController class]])
                {
                    MIChangeLanguageViewController *languageVC = [[MIChangeLanguageViewController alloc] initWithNibName:@"MIChangeLanguageViewController" bundle:nil];
                    languageVC.view.tag = 100;
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:languageVC]];
                    
                }
                break;
            }
            case 4:
            {
               if(![[nav.viewControllers firstObject].title isEqualToString:arrSideMenu[indexPath.row]])
                {
                    MIWebViewViewController *webView = [[MIWebViewViewController alloc] initWithNibName:@"MIWebViewViewController" bundle:nil];
                    webView.title = CLocalize(arrSideMenu[indexPath.row]);
                    [webView setGeneralDataType:GeneralDataTypeAboutSmashPoint];
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:webView]];
                }

                break;
            }
            case 5:
            {
               if(![[nav.viewControllers firstObject].title isEqualToString:arrSideMenu[indexPath.row]])
                {
                    MIWebViewViewController *webView = [[MIWebViewViewController alloc] initWithNibName:@"MIWebViewViewController" bundle:nil];
                    webView.title = CLocalize(arrSideMenu[indexPath.row]);
                    [webView setGeneralDataType:GeneralDataTypeAboutUs];
                    [appDelegate.sidePanelViewController setRootViewController:[UINavigationController navigationControllerWithRootViewController:webView]];
                }

                break;
            }
            case 6:
            {
                
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL URLWithString:CFooterContactUsUrl];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                break;
            }
            case 7:
            {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"ZURU_Privacy_Policy" ofType:@"pdf"];
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL fileURLWithPath:path];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                break;
            }
            case 8:
            {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"Childrens_Privacy_Policy" ofType:@"pdf"];
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL fileURLWithPath:path];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                break;
            }
            case 9:
            {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"Terms_Conditions" ofType:@"pdf"];
                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
                inAppWebVC.iObject = [NSURL fileURLWithPath:path];
                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                break;
            }
            case 10:
            {
//                MIInAppWebViewController *inAppWebVC = [MIInAppWebViewController initWithXib];
//                inAppWebVC.iObject = [NSURL URLWithString:CSmasherWorld_Com];
//                [nav presentViewController:[UINavigationController navigationControllerWithRootViewController:inAppWebVC] animated:YES completion:nil];
                [self openURLInSafari:CSmasherWorld_Com];
                break;
            }
            
        }
        if(indexPath.row != 1 || indexPath.row != 2 )
            [appDelegate.sidePanelViewController hideRightViewAnimated:YES completionHandler:nil];
    }

}

- (void)openURLInSafari:(NSString *)url
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]])
    {
        if (IosVersion < 10)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:nil];
            
        }
        
    }
}


@end

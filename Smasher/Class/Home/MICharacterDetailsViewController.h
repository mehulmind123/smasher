//
//  MICharacterDetailsViewController.h
//  Smasher
//
//  Created by Mac-00014 on 28/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"

@interface MICharacterDetailsViewController : MIParentViewController
{
    IBOutlet UIView *vWNoDataFound;
    IBOutlet UILabel *lblCharName;
    IBOutlet UILabel *lblPoint;
    IBOutlet UILabel *lblCode;
    IBOutlet UILabel *lblDescription;
    IBOutlet UILabel *lblAvailable;
    IBOutlet UILabel *lblSeries;
    IBOutlet UILabel *lblRarity;
    IBOutlet UILabel *lblFinish;
    IBOutlet UILabel *lblRange;
    IBOutlet UILabel *lblTeam;
    IBOutlet UILabel *lblSP;
    
    IBOutlet UIButton *btnIOwn;
    IBOutlet UIButton *btnIWant;
    
    IBOutlet UIView *vWChar;
    IBOutlet UIImageView *imgVChar;
    IBOutlet UIView *vWFooter;
    
    IBOutlet UICollectionView *collView;
    IBOutlet NSLayoutConstraint *cnHeightCollView;
    IBOutlet NSLayoutConstraint *cnRatioNoDataView;
}

@end

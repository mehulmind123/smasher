//
//  MICharacterDetailsViewController.m
//  Smasher
//
//  Created by Mac-00014 on 28/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MICharacterDetailsViewController.h"
#import "MIFooterView.h"
#import "MIPackCollectionViewCell.h"
@interface MICharacterDetailsViewController ()<MIBubbleViewLayoutDelegate>
{
    MIFooterView *footer;
    NSArray *arrPack;
    NSDictionary *dictDetails;
}
@end

@implementation MICharacterDetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    [UIApplication sharedApplication].statusBarHidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    [UIApplication sharedApplication].statusBarHidden = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    [self setStatusBarHidden:YES];
    //...
    vWChar.layer.cornerRadius = 10;
    vWChar.layer.shadowColor = ColorBlack_000000.CGColor;
    vWChar.layer.shadowOffset = CGSizeZero;
    vWChar.layer.shadowRadius = 3.0f;
    vWChar.layer.shadowOpacity = 0.5f;
    
    [self.navigationController.navigationBar setHidden:YES];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        footer = [MIFooterView footer];
        [vWFooter addSubview:footer];
    });
    
   // arrPack = @[@"1 Pack",@"5 Pack",@"12 Pack"];
    //...UICollectionView
    MIBubbleViewLayout *layout = [[MIBubbleViewLayout alloc] initWithDelegate:self];
    [layout setMinimumLineSpacing:6.0f];
    [layout setMinimumInteritemSpacing:6.0f];
    
    [collView setCollectionViewLayout:layout];
    [collView registerNib:[UINib nibWithNibName:@"MIPackCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIPackCollectionViewCell"];
    
    [collView performBatchUpdates:nil completion:^(BOOL finished)
    {
        cnHeightCollView.constant = collView.contentSize.height;
        
    }];
    
    //if(_isFromIWant)
      //  [self btnSwitchClicked:_isFromIWant == 1 ? btnIWant : btnIOwn];
    
    [self loadDataFromServer];
}


#pragma mark -
#pragma mark - Action Event

- (IBAction)btnCloseClicked:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSwitchClicked:(UIButton*)sender
{
    if(!appDelegate.loginUser)
    {
        [appDelegate showLoginRequirePopUp:self];
        
        return;
    }
    
    if(sender.selected)
    {
        [self changeCharacterStatus:CCharStatusNone];
    }
    else if(sender == btnIWant)
    {
        [self changeCharacterStatus:CCharStatusWant];
    }
    else if (sender == btnIOwn)
    {
        [self changeCharacterStatus:CCharStatusOwn];
    }

}

- (void)changeCharacterStatus:(NSUInteger)status
{
    [self switchSelection:status];
    
    [[APIRequest request] changeCharacterStatusWithCharId:[dictDetails stringValueForJSON:@"id"] withStatus:status completed:^(id responseObject, NSError *error)
     {
         if([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
         {
             if(self.block)
                 self.block([NSNumber numberWithInteger:status],nil);
             
         }
         else
         {
             [self switchSelection:[dictDetails integerForKey:@"character_status"]];
         }
     }];
}
#pragma mark
#pragma mark - Load data

- (void)loadDataFromServer
{
    [vWNoDataFound setHidden:NO];
    [cnRatioNoDataView setPriority:999];
    [self startLoadingAnimationInView:vWNoDataFound];
    [[APIRequest request] loadCharcterDetails:self.iObject completed:^(id responseObject, NSError *error)
    {
        if([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            dictDetails = [responseObject valueForKey:CJsonData];
            [self manageResponse];
        }
        
        [self stopLoadingAnimationInView:vWNoDataFound type:[dictDetails count] ? StopAnimationTypeRemove : error ? StopAnimationTypeErrorTapToRetry : StopAnimationTypeDataNotFound message:@"" touchUpInsideClickedEvent:^{

            [self loadDataFromServer];
        }];
       
    }];
}

- (void)manageResponse
{
    [vWNoDataFound setHidden:YES];
    [cnRatioNoDataView setPriority:250];
    if([dictDetails count])
    {
        
        [lblCharName setText:[dictDetails stringValueForJSON:@"character_name"]];
        [lblCode setText:[dictDetails stringValueForJSON:@"code"]];
        [lblPoint setText:[NSString stringWithFormat:@"%@ S.P",[dictDetails stringValueForJSON:@"value"]]];
        [lblDescription setText:[dictDetails stringValueForJSON:@"description"]];
        [lblSeries setText:[dictDetails stringValueForJSON:@"series_name"]];
        [lblRange setText:[dictDetails stringValueForJSON:@"range_name"]];
        [lblRarity setText:[dictDetails stringValueForJSON:@"rarity_name"]];
        [lblTeam setText:[dictDetails stringValueForJSON:@"team_name"]];
        [lblFinish setText:[dictDetails stringValueForJSON:@"finish"]];
        [lblSP setText:[dictDetails stringValueForJSON:@"value"]];
        [lblAvailable setText:[dictDetails stringValueForJSON:@"available_only"]];
        
        [imgVChar sd_setImageWithURL:[appDelegate imageURL:[dictDetails stringValueForJSON:@"image"] withSize:imgVChar.frame.size]];
        
        [self switchSelection:[dictDetails integerForKey:@"character_status"]];
        arrPack = [[dictDetails stringValueForJSON:@"found_name"] componentsSeparatedByString:@","];
        [collView reloadData];
        
        [collView performBatchUpdates:nil completion:^(BOOL finished)
         {
             cnHeightCollView.constant = collView.contentSize.height;
             
         }];
        
    }

}

- (void)switchSelection:(NSInteger)status
{
    switch (status)
    {
        case CCharStatusOwn:
        {
            btnIOwn.selected = YES;
            [btnIOwn setTitle:@"" forState:UIControlStateSelected];
            btnIWant.selected = NO;
            break;
        }
        case CCharStatusWant:
        {
            btnIOwn.selected = NO;
            btnIWant.selected = YES;
            [btnIWant setTitle:@"" forState:UIControlStateSelected];
            break;
        }
            
        default:
        {
            btnIOwn.selected =
            btnIWant.selected = NO;
            break;
        }
            
    }
}
#pragma mark
#pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrPack.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *indentifier = @"MIPackCollectionViewCell";
    MIPackCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
    
    [cell.lblPackName setText:arrPack[indexPath.row]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView itemSizeAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = arrPack[indexPath.row];
    CGSize size = [title sizeWithAttributes:@{NSFontAttributeName:(IS_IPAD ? CFontGroBold(20) : CFontGroBold(16))}];
    size.height = (IS_IPAD ? 36 : 27);
    size.width += CTextPadding*2;
    
    if (size.width > CViewWidth(collectionView))
        size.width = CViewWidth(collectionView);
    
    return size;
}


@end

//
//  MIRightPanelViewController.h
//  Smasher
//
//  Created by Mac-00014 on 26/09/17.
//  Copyright © 2017 Ishwar-00014. All rights reserved.
//

#import "MIParentViewController.h"

@interface MIRightPanelViewController : MIParentViewController
{
    IBOutlet UITableView *tblSideMenu;
    
    IBOutlet UIButton *btnIOwn;
    IBOutlet UIButton *btnIWant;
    IBOutlet UIButton *btnEditProfile;
        
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblYouSmashPoint;
    
    IBOutlet UIView *vWIOwnAndIWant;
    IBOutlet UIImageView *imgVLogo;
    
}
@end
